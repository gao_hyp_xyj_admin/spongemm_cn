---
title: SPONGE
description: Simulation Package tOward Next GEneration molecular modelling
published: true
date: 2024-02-22T07:59:46.935Z
tags: 
editor: markdown
dateCreated: 2024-02-22T07:12:04.378Z
---

> The English version of the website is still under construction.
> The tutorials and documents have not been translated yet.
{.is-info}


# Introduction

SPONGE (Simulation Package tOward Next GEneration molecular modelling) is a molecular simulation package developed by the research group of Yi Qin Gao at Peking University.

## Basic Introduction
Molecular dynamics (MD) simulation is a useful tool in chemistry, physics, biology, materials science, and many other fields. Over the past 40 years, various efficient computational algorithms and MD simulation packages have been developed for studying the dynamics of increasingly complex and large systems, such as RNA polymerases, membrane proteins in cell membranes, and the SARS-CoV-2 virus. However, as the scope and scale of applications have expanded, molecular simulation software requires higher computational power. One of the most direct strategies to narrow the gap between simulation and experiment is to utilize more powerful computing hardware. For example, the Shaw Institute designed Anton specifically for simulating single-domain proteins with hundreds of atoms at millisecond-level simulations. In comparison, using graphics processing units (GPUs) may be the most cost-effective and promising approach for most research groups. On the other hand, many advanced computational algorithms have also been developed and widely applied, extending the time scale of simulations. In particular, over the past few decades, many enhanced sampling methods have been developed to achieve rapid thermodynamic and/or kinetic calculations. These methods include, but are not limited to, widely used umbrella sampling, metadynamics, accelerated MD, replica exchange molecular dynamics (REMD), parallel tempering, simulation annealing, multiple canonical simulations (especially implemented via the Wang-Landau algorithm), and many others.

Over the past 15 years, we have been dedicated to developing efficient molecular simulation methods for complex chemical and biological systems. We have designed a series of enhanced sampling methods, achieved rapid sampling of conformational and trajectory spaces, and realized fast calculations of thermodynamic and dynamic properties of complex systems. Recently, we have developed a domestically produced MD simulation package called SPONGE, which not only implements GPU-accelerated traditional MD simulations but also implements efficient enhanced sampling methods proposed by our research group. This software package is highly modular and can easily integrate other functionalities or algorithms, especially the latest deep learning potentials and algorithms.

## Components
Currently, SPONGE mainly consists of three parts, namely CudaSPONGE, MindSPONGE, and Xponge.

- CudaSPONGE: Molecular dynamics simulation program written in CUDA C/C++
- MindSPONGE: Molecular dynamics simulation program written using the Mindspore neural network framework
- Xponge: Molecular dynamics simulation pre- and post-processing tools written in Python

# References
## Programs
### CudaSPONGE

> Yu-Peng Huang,  Yijie Xia,  Lijiang Yang,  Jiachen Wei,  Yi Isaac Yang,  Yi Qin Gao, **SPONGE: A GPU-Accelerated Molecular Dynamics Package with Enhanced Sampling and AI-Driven Algorithms**. *Chin. J. Chem.*, **2022**, 40: 160-168.
DOI: [10.1002/cjoc.202100456](https://doi.org/10.1002/cjoc.202100456).

### MindSPONGE

> Jun Zhang, Dechin Chen, Yijie Xia, Yu-Peng Huang, Xiaohan Lin, Xu Han, Ningxi Ni, Zidong Wang, Fan Yu, Lijiang Yang, Yi Isaac Yang, and Yi Qin Gao, **Artificial Intelligence Enhanced Molecular Simulations**. *J. Chem. Theory Comput.* **2023** Jul 25;19(14):4338-4350.
DOI: [10.1021/acs.jctc.3c00214](https://doi.org/10.1021/acs.jctc.3c00214).

### Xponge

> Yijie Xia, and Yi Qin Gao, **Xponge: A Python package to perform pre- and post-processing of molecular simulations**. *J. Open Source Softw.*, **2022**, 7(77), 4467,
DOI: [10.21105/joss.04467](https://doi.org/10.21105/joss.04467)

## Applications

### 2024

> Yu-Peng Huang, Yijie Xia, Lijiang Yang, and Yi Qin Gao, **PMC-IZ: A Simple Algorithm for the Electrostatics Calculation in Slab Geometric Molecular Dynamics Simulations**, *J. Chem. Theory Comput.*, **2024**, 20, 2, 832–841
![2024-1.jpeg](/首页/2024-1.jpeg)
DOI: [10.1021/acs.jctc.3c01124](https://doi.org/10.1021/acs.jctc.3c01124)

### 2023

> Ye Tian, Yizhi Song, Yijie Xia, Jiani Hong, Yupeng Huang, Runze Ma, Sifan You, Dong Guan, Duanyun Cao, Mengze Zhao, Ji Chen, Chen Song, Kaihui Liu, Li-Mei Xu, Yi Qin Gao, En-Ge Wang and Ying Jiang, **Nanoscale one-dimensional close packing of interfacial alkali ions driven by water-mediated attraction**, *Nat. Nanotechnol.*, **2023**
![2023-4.png](/首页/2023-4.png)
DOI: [10.1038/s41565-023-01550-9](https://doi.org/10.1038/s41565-023-01550-9)

> Dejia Liu, Hong Zhang, Yu-Peng Huang, and Yi Qin Gao, **Investigating the Activation Mechanism Differences between Human and Mouse cGAS by Molecular Dynamics Simulations**, *J. Phys. Chem. B*, **2023**, 127, 22, 5034–5045.
![2023-3.gif](/首页/2023-3.gif)
DOI: [10.1021/acs.jpcb.3c02377](https://doi.org/10.1021/acs.jpcb.3c02377)

> Shizhi Huang, Yu-Peng Huang, Yijie Xia, Jingyi Ding, Chengyuan Peng, Lulu Wang, Junrong Luo, Xin-Xiang Zhang, Junrong Zheng, Yi Qin Gao, and Jitao Chen, **High Li + coordinated solvation sheaths enable high‐quality Li metal anode**, *InfoMat*, **2023**, 5(5):e12411.
![2023-2.jpg](/首页/2023-2.jpg)
DOI: [10.1002/inf2.12411](https://doi.org/10.1002/inf2.12411)

> Hongxu Du, Wenjing Zhao, Yijie Xia, Siyu Xie, Yi Tao, Yi Qin Gao, Jie Zhang, and Xinhua Wan, **Effect of stereoregularity on excitation-dependent fluorescence and room-temperature phosphorescence of poly(2-vinylpyridine)**. *Aggregate*, **2023**, 4, e276.
![2023-1.png](/首页/2023-1.png)
DOI: [10.1002/agt2.276](https://doi.org/10.1002/agt2.276)

### 2022

> Wenjing Zhao, Hongxu Du, Yijie Xia, Siyu Xie, Yu-Peng Huang, Tieqi Xu, Jie Zhang, Yi Qin Gao and Xinhua Wan, **Accelerating supramolecular aggregation by molecular sliding**, *Phys. Chem. Chem. Phys.*, **2022**,24, 23840-23848.
![2022-1.gif](/首页/2022-1.gif)
DOI: [10.1039/D2CP04064F](https://doi.org/10.1039/D2CP04064F)

# Website Updates
## 2024

### Feburary 22, 2024
Starting construction of the English version of the homepage

### January 1, 2024
Release of version 1.4 of SPONGE

## 2023
### December 29, 2023
Website framework updated, now using wiki.js

### November 25, 2023
SPONGE 1.4b0 version update

Xponge documentation updated

SPONGE documentation updated

### March 12, 2023
SPONGE version 1.3 update

## 2022
### August 13, 2022
SPONGE version 1.2.6 released, supporting non-periodic boundary conditions and implicit solvent simulation

Xponge documentation updated

### April 1, 2022
SPONGE version 1.2.5 update

### January 7, 2022
SPONGE version 1.2beta3 update

## 2021
### November 9, 2021
SPONGE version 1.2beta2 update

### October 21, 2021
SPONGE version 1.2beta update

### August 21, 2021
Successful MindSpore SPONGE Workshop

### June 15, 2021
Website successfully filed for ICP and web security, program uploads, basic construction completed





