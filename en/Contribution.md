---
title: Contribution
description: 
published: true
date: 2024-02-22T07:57:41.156Z
tags: 
editor: markdown
dateCreated: 2024-02-22T07:57:41.156Z
---

# Contribute to This Website

## Gitee
The content of this website is stored and hosted on [Gitee repository](https://gitee.com/gao_hyp_xyj_admin/spongemm_cn.git). You can contribute by initiating pull requests to the Gitee repository.

## Internal Method
This website allows registration. Once registered, and after identity verification, you can use the internal editor to edit the website.

# Contribute to SPONGE
## Gitee
Currently, all parts of SPONGE are hosted on Gitee.

- [CudaSPONGE](https://gitee.com/gao_hyp_xyj_admin/SPONGE.git)
- [Xponge](https://gitee.com/gao_hyp_xyj_admin/xponge.git)
- [MindSPONGE](https://gitee.com/helloyesterday/mindsponge)