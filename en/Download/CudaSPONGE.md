---
title: CudaSPONGE Downloads
description: 
published: true
date: 2024-02-22T07:23:10.945Z
tags: 
editor: markdown
dateCreated: 2024-02-22T07:23:10.945Z
---

# Stable Version

Current stable version
[CudaSPONGE-v1.4](/程序包/sponge_v1.4.zip)

# Deprecated Versions

Versions no longer maintained

[CudaSPONGE-v1.3](/程序包/sponge_v1.3.zip)
[CudaSPONGE-v1.2](/程序包/sponge_v1.2.6.zip)
[CudaSPONGE-v1.1](/程序包/sponge_nmd_pure_v1.1.zip)
