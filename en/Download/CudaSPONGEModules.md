---
title: CudaSPONGE Modules
description: 
published: true
date: 2024-02-22T07:27:23.029Z
tags: 
editor: markdown
dateCreated: 2024-02-22T07:27:23.029Z
---

# Unupdated Modules
These modules here cannot be directly used in the latest stable version of SPONGE and may require slight modifications.

## FGM Double Layer

[fgm_double_layer_mod_v2.zip](/模块包/fgm_double_layer_mod_v2.zip)

## XRD3D

[xrd3d_meta1d_mod.zip](/模块包/xrd3d_meta1d_mod.zip)

## DPD

[dpd_and_spring_cuboid_mod.zip](/模块包/dpd_and_spring_cuboid_mod.zip)

## Python Interface

[pyplugin.zip](/模块包/pyplugin.zip)

# Integrated Modules
These modules have been integrated into the latest stable version of SPONGE. Links are provided here for compatibility with older versions.

## External Forces

[exforce.zip](/模块包/exforce.zip)

## 1D Metadynamics

[meta1d_mod.zip](/模块包/meta1d_mod.zip)

## Classic SITS

[classic_sits_mod_v1p2p5.zip](/模块包/classic_sits_mod_v1p2p5.zip)

## CMAP

[cmap_module.zip](/模块包/cmap_module.zip)





