---
title: SPONGE工具
description: 
published: true
date: 2024-03-19T08:38:01.358Z
tags: 
editor: markdown
dateCreated: 2023-12-29T00:18:16.058Z
---

# Xponge

## 介绍

- Xponge主要功能为产生CudaSPONGE所需的分子动力学模拟初始输入

- Xponge附带一定的前后处理功能

- Xponge能够为复杂的分子动力学模拟产生自动化工作流

- Xponge能够为提供一定的通用分子处理功能

## 安装
使用pip进行安装：
`pip install Xponge`

# VMD插件

## 介绍

CudaSPONGE的VMD插件可以使VMD将CudaSPONGE的构象和轨迹进行可视化

## 安装

点击下方的链接下载，然后按照内部的README.md进行安装
[sponge_vmd 1.4.3版本](/工具包/sponge_vmd_v1.4.3.zip)