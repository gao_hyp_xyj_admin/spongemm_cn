---
title: CudaSPONGE模块
description: 
published: true
date: 2023-12-29T00:09:54.526Z
tags: 
editor: markdown
dateCreated: 2023-12-29T00:04:08.826Z
---

# 未更新的模块

此处的模块不能直接在最新稳定版本的SPONGE中直接使用，需要稍加修改

## FGM双层平板

[fgm_double_layer_mod_v2.zip](/模块包/fgm_double_layer_mod_v2.zip)

## XRD3D

[xrd3d_meta1d_mod.zip](/模块包/xrd3d_meta1d_mod.zip)

## DPD

[dpd_and_spring_cuboid_mod.zip](/模块包/dpd_and_spring_cuboid_mod.zip)

## python接口

[pyplugin.zip](/模块包/pyplugin.zip)

# 已整合的模块

此处的模块已整合至最新稳定版本的SPONGE中，在此处提供链接以兼容老版本

## 外力

[exforce.zip](/模块包/exforce.zip)

## 1维metadynamics

[meta1d_mod.zip](/模块包/meta1d_mod.zip)

## 经典SITS

[classic_sits_mod_v1p2p5.zip](/模块包/classic_sits_mod_v1p2p5.zip)

## cmap

[cmap_module.zip](/模块包/cmap_module.zip)