# Xponge.mdrun

This **package** helps to run the molecular dynamics simulation

## subpackages


## submodules


## functions

### run

call SPONGE to run the MD simulation


parameters | explanation
--- | ---
args | a string or a list of arguments


return | None
---|---

