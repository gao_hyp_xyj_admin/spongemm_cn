# Xponge.helper.math

This **module** is used to provide basic common math functions

## subpackages


## submodules


## functions

### get_rotate_matrix

This **function** is used to get the rotation matrix by the axis and the angle


parameters | explanation
--- | ---
r0 | the axis to rotate around
angle | the angle to rotate


return | the rotation matrix
---|---

### kabsch

This **function** uses Kabsch algorithm to align two sets of positions


parameters | explanation
--- | ---
positions1 | the first set of positions, a numpy array with shape (N,3)
positions2 | the second set of positions, a numpy array with shape (N,3)


return | a tuple of 3 numpy arrays
---|---

### get_fibonacci_grid

This **function** is used to get the sphere grid by fibonacci algorithm


parameters | explanation
--- | ---
n | the number of grids
origin | the origin of the sphere
radius | the radius of the sphere


return | a numpy array of grid coordinates
---|---

### guess_element_from_mass

This **function** is used to guess element from its mass


parameters | explanation
--- | ---
mass | an int or a float, the mass


return | the element name
---|---

### get_basis_vectors_from_length_and_angle

This **function** is used to get the basis vectors from the cell lengths and cell angles.


parameters | explanation
--- | ---
a | the length of the first basis vector
b | the length of the second basis vector
c | the length of the third basis vector
alpha | the angle between the second and the third basis vectors
beta | the angle between the first and the third basis vectors
gamma | the angle between the first and the second basis vectors
angle_in_degree | whether the unit of the angles is degree, True for default.


return | a 3x3 numpy array, and every row is one basis vector
---|---

