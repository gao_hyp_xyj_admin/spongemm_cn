# Xponge.helper.rdkit

This **module** gives the interface to the package RDKit

## subpackages


## submodules


## functions

### assign_to_rdmol

This **function** is used to convert an Xponge.Assign to a RDKit.rdMol


parameters | explanation
--- | ---
assign | the Assign instance
ignore_bond_type | set the bond type always to UNSPECIFIED


return | the RDKit.rdMol instance
---|---

### rdmol_to_assign

This **function** is used to convert a RDKit.rdMol to an Xponge.Assign


parameters | explanation
--- | ---
rdmol | the RDKit.rdMol instance


return | the Xponge.Assign instance
---|---

### insert_atom_type_to_rdmol

This **function** inserts the atom types in the force field to the RDKit.rdmol instance.


parameters | explanation
--- | ---
mol | the RDKit.rdmol instance
res | the Residue instance corresponding to ``mol``
assign | the Assign instance corresponding to ``mol``
atom_type_dict | the dict mapping the atom type to the isotope number


return | None
---|---

### find_equal_atoms

This **function** is used to find the chemical equivalent atoms in the molecule


parameters | explanation
--- | ---
assign | the Assign instance


return | a list of equalvalent atom index lists
---|---

