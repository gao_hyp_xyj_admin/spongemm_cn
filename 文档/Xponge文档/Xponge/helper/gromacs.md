# Xponge.helper.gromacs

This **module** contains functions to interact with gromacs

## subpackages


## submodules


## functions

### sort_atoms_by_gro

This **function** sorts the atoms in a Xponge.Molecule according to the index in a gro file


parameters | explanation
--- | ---
mol | a Xponge.Molecule
gro | the gro file

### rtp_to_mol2

This **function** convert a rtp file into several mol2 files


parameters | explanation
--- | ---
rtp | the name of the rtp file
prefix | the output prefix for the mol files

### read_tdb

This **function** reads a tdb file and according to the rule modify the mol2 file


parameters | explanation
--- | ---
tdb | the name of the tdb file
rule | the rule to use
mol2_in | the input mol2 file
mol2_out | the output mol2 file
newname | the new name of the residue

