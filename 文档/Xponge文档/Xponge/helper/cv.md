# Xponge.helper.cv

This **module** helps with the definition of CVs in SPONGE

## subpackages


## submodules


## functions

### to_string

convert this information to a string

### to_string

convert this information to a string

### to_string

convert this information to a string

## classes

### CVSystem

This **class** is used to help with the definition of CVs in SPONGE


parameters | explanation
--- | ---
molecule | the molecule to define the CVs

#### u

the MDAnalysis.Universe of the molecule

#### id2index

the MDAnalysis.Universe of the molecule

#### get_atom_index

Convert an Xponge.Atom to int

#### remove

Remove a name from the system

#### add_center

Add a virtual atom with the type of "center" to the system


parameters | explanation
--- | ---
name | the name of the virtual atom
select | a selection string of MDAnalysis
weight | weight of the atoms, None for 1/N


return | None
---|---

#### add_cv_position

Add a CV with the type of "position" to the system


parameters | explanation
--- | ---
name | the name of the CV
atom | an int or a name of virtual atom
xyz | the axis of the position
scaled | whether the posithion need to be scaled


return | None
---|---

#### add_cv_distance

Add a CV with the type of "distance" to the system


parameters | explanation
--- | ---
name | the name of the CV
atom1 | an int or a name of virtual atom
atom2 | an int or a name of virtual atom


return | None
---|---

#### add_cv_displacement

Add a CV with the type of "displacement" to the system


parameters | explanation
--- | ---
name | the name of the CV
atom1 | an int or a name of virtual atom
atom2 | an int or a name of virtual atom
xyz | the axis of the position


return | None
---|---

#### add_cv_box_length

Add a CV with the type of "box_length" to the system


parameters | explanation
--- | ---
name | the name of the CV
xyz | the axis of the position


return | None
---|---

#### add_cv_density

Add a CV with the type of "combination" to the system, which gives the density of the system


parameters | explanation
--- | ---
name | the name of the CV


return | None
---|---

#### add_cv_angle

Add a CV with the type of "angle" to the system


parameters | explanation
--- | ---
name | the name of the CV
atom1 | an int or a name of virtual atom
atom2 | an int or a name of virtual atom
atom3 | an int or a name of virtual atom


return | None
---|---

#### add_cv_dihedral

Add a CV with the type of "dihedral" to the system


parameters | explanation
--- | ---
name | the name of the CV
atom1 | an int or a name of virtual atom
atom2 | an int or a name of virtual atom
atom3 | an int or a name of virtual atom
atom4 | an int or a name of virtual atom


return | None
---|---

#### add_cv_rmsd

Add a CV with the type of "displacement" to the system


parameters | explanation
--- | ---
name | the name of the CV
select | a string of selection


return | None
---|---

#### print

Add a CV to print


parameters | explanation
--- | ---
name | the name of the CV


return | None
---|---

#### steer

Add a CV to steer


parameters | explanation
--- | ---
name | the name of the CV
weight | the weight for steering


return | None
---|---

#### restrain

Add a CV to restrain


parameters | explanation
--- | ---
name | the name of the CV
weight | the weight for restraints
reference | the reference for restraints
start_step | the step to start the restraints
max_step | the step to reach the max weight of restraints
max_step | the step to reduce the weight of restraints
stop_step | the step to stop the restraints


return | None
---|---

#### meta1d

Add a CV to do metadynamics


parameters | explanation
--- | ---
name | the name of the CV
dCV | the weight for meta1d
CV_minimal | the minimal value of the CV
CV_maximum | the maximum value of the CV
welltemp_factor | the welltemfactor value of the CV
height | the height of the Gaussian potential to add
sigma | the sigma of the Gaussian potential to add

#### output

Output the recorded system to a file


parameters | explanation
--- | ---
filename | the name of the output file for cv_in_file
folder | the folder of the output files, the current working folder for default


return | None
---|---

