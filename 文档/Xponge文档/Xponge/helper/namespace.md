# Xponge.helper.namespace

This **module** is used to provide help functions and classes about namespace

## subpackages


## submodules


## functions

### source

This **function** import the module and merge all the global variables into the caller module globals().


parameters | explanation
--- | ---
module | the module name to import
into_global | whether to merge the global variables
reload_module | whether to reload the module


return | the module to import
---|---

### set_real_global_variable

This **function** is used to set the variable to real global variable


parameters | explanation
--- | ---
name | the name to the use as a global variable
value | the value corresponding to the variable


return | None
---|---

### remove_real_global_variable

This **function** is used to remove the variable from real global variable


parameters | explanation
--- | ---
name | the global variable to remove


return | None
---|---

### set_alternative_name

This **function** is used to set the alternative names for a function to an object.


parameters | explanation
--- | ---
obj | the object
func | the function
set_method | the method to set


return | None
---|---

### set_classmethod_alternative_names

This **function** is used to set the attribute/method alternative names for a class


parameters | explanation
--- | ---
cls | the class to set alternative names


return | None
---|---

### set_attribute_alternative_names

.. deprecated:: 1.3.1


parameters | explanation
--- | ---
instance | the instance to set names


return | None
---|---

### set_global_alternative_names

This **function** is used to set the alternative names of the functions in the module to be global


parameters | explanation
--- | ---
real_global | make the variable to real global, which can be used without the module name


return | None
---|---

