# Xponge.helper.file

This **module** helps to process the files of molecular modelling

## subpackages


## submodules


## functions

### file_filter

This **function** finds the lines which contains any of the given regular expressions and replace some parts.


parameters | explanation
--- | ---
infile | the input file or filename
outfile | the output file or filename
reg_exp | a list of regular expressions. Lines which match any regular expressions will be kept.
replace_dict | a dict of regular expressions and the replacement

### pdb_filter

This **function** finds the lines in pdb which meets the need


parameters | explanation
--- | ---
infile | the input file or filename
outfile | the output file or filename
head | a list of heads which will be included
hetero_residues | a list of hetero residue names which will be included
chains | a list of the code for the chains you need. None for all (default).
rename_ions | a dict to rename the ions

