# Xponge.tools.ti

This **module** contains the functions for ti analysis

## subpackages


## submodules


## functions

### ti_analysis

This **function** is used to do the ti analysis


parameters | explanation
--- | ---
args | the arguments from the command line


return | None
---|---

