# Xponge.tools.unittests.test_3_charmm27

This **module** includes unittests of the Xponge.forcefield.charmm27

## subpackages


## submodules


## functions

### test_protein

test the single point energy for residues of protein

### test_dna

test the single point energy for residues of DNA

### test_rna

test the single point energy for residues of RNA

