# Xponge.tools.unittests.test_1_cif

This **module** includes unittests of the cif files

## subpackages


## submodules


## functions

### test_cof

Test loading a cif file for Covalent Organic Framework

### test_som

Test loading a cif file for Small Organic Molecule

