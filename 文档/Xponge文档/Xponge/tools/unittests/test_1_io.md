# Xponge.tools.unittests.test_1_io

This **module** includes unittests of the basic functions in Xponge.load and Xponge.build

## subpackages


## submodules


## functions

### test_pdb_general

test loading a pdb file

### test_pdb_ssbond_link_and_conect

test read the conect and the ssbond part of PDB file

### test_mol2_general

test loading a mol2 file

