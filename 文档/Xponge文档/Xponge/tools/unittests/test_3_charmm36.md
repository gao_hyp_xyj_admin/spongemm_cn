# Xponge.tools.unittests.test_3_charmm36

This **module** includes unittests of the Xponge.forcefield.charmm36

## subpackages


## submodules


## functions

### test_protein

test the single point energy for residues of protein

### test_rna

test the single point energy for residues of RNA

### test_dna

test the single point energy for residues of DNA

