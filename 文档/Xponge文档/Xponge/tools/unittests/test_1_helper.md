# Xponge.tools.unittests.test_1_helper

This **module** includes unittests of the basic functions of Xponge.helper

## subpackages


## submodules


## functions

### test_adding_of_residues

test adding and multiplying of Xponge.ResidueType and Xponge.Moleule

### test_omitting_residue_type

test omitting atoms for a residue type

### test_reseting_type

test change the type of certain residue

### test_pdb_filter

test the pdb filter

