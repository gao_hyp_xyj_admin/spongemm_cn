# Xponge.tools.unittests.test_1_process

This **module** includes unittests of the basic functions of Xponge.process

## subpackages


## submodules


## functions

### test_impose

test imposing bonds, angles and dihedrals

### test_solvent_process

test the function to add solvents box

### test_lattice

test the lattice building system

