# Xponge.tools.unittests.test_5_wrap

This **module** gives the unit tests of the wrapping molecules

## subpackages


## submodules


## functions

### test_unperiodic

test the wrapping of unperiodic molecules

### test_periodic

test the wrapping of periodic molecules

