# Xponge.tools.unittests.test_1_assign

This **module** includes unittests of the Xponge.Assign operators

## subpackages


## submodules


## functions

### test_get_assign

Test the functions to get assignment

### test_objective_assign_rule

test creating a new rule to assign the Xponge.AtomType of one atom

### test_residuetype_to_assign

test convert an Xponge.ResidueType to Xponge.Assign

### test_string_assign_rule

test creating a new rule to assign the string of one atom

### test_ring_system

test the basic functions the the helper class _RING

### test_atom_deletion

test the function to delete an atom from an Xponge.Assign

### test_bond_deletion

test the function to delete a bond from an Xponge.Assign

### test_equal_atom_search

test the function to find the equal atoms in a molecule

### test_atom_type_determination

test the function to find the atom type

### test_uff_optimization

test the optimization of the molecule using UFF

### test_saving

test the function to save a Xponge.assign

