# Xponge.tools.mbar

This **module** contains the functions for mbar analysis

## subpackages


## submodules


## functions

### mbar_analysis

This **function** is used to do the mbar analysis


parameters | explanation
--- | ---
args | the arguments from the command line


return | None
---|---

