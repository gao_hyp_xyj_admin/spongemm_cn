# Xponge.tools.unittests

This **module** gives the unit tests of the functions in Xponge

## subpackages


## submodules


### [Xponge.tools.unittests.test_0_base](/文档/Xponge文档/Xponge/tools/unittests/test_0_base)


### [Xponge.tools.unittests.test_1_assign](/文档/Xponge文档/Xponge/tools/unittests/test_1_assign)


### [Xponge.tools.unittests.test_1_charge](/文档/Xponge文档/Xponge/tools/unittests/test_1_charge)


### [Xponge.tools.unittests.test_1_cif](/文档/Xponge文档/Xponge/tools/unittests/test_1_cif)


### [Xponge.tools.unittests.test_1_helper](/文档/Xponge文档/Xponge/tools/unittests/test_1_helper)


### [Xponge.tools.unittests.test_1_io](/文档/Xponge文档/Xponge/tools/unittests/test_1_io)


### [Xponge.tools.unittests.test_1_process](/文档/Xponge文档/Xponge/tools/unittests/test_1_process)


### [Xponge.tools.unittests.test_2_ffbase](/文档/Xponge文档/Xponge/tools/unittests/test_2_ffbase)


### [Xponge.tools.unittests.test_3_bsc1](/文档/Xponge文档/Xponge/tools/unittests/test_3_bsc1)


### [Xponge.tools.unittests.test_3_charmm27](/文档/Xponge文档/Xponge/tools/unittests/test_3_charmm27)


### [Xponge.tools.unittests.test_3_charmm36](/文档/Xponge文档/Xponge/tools/unittests/test_3_charmm36)


### [Xponge.tools.unittests.test_3_ff14sb](/文档/Xponge文档/Xponge/tools/unittests/test_3_ff14sb)


### [Xponge.tools.unittests.test_3_ff19sb](/文档/Xponge文档/Xponge/tools/unittests/test_3_ff19sb)


### [Xponge.tools.unittests.test_3_ol3](/文档/Xponge文档/Xponge/tools/unittests/test_3_ol3)


### [Xponge.tools.unittests.test_3_oplsaam](/文档/Xponge文档/Xponge/tools/unittests/test_3_oplsaam)


### [Xponge.tools.unittests.test_4_min_efficiency](/文档/Xponge文档/Xponge/tools/unittests/test_4_min_efficiency)


### [Xponge.tools.unittests.test_4_npt_efficiency](/文档/Xponge文档/Xponge/tools/unittests/test_4_npt_efficiency)


### [Xponge.tools.unittests.test_4_nvt_efficiency](/文档/Xponge文档/Xponge/tools/unittests/test_4_nvt_efficiency)


### [Xponge.tools.unittests.test_5_nopbc](/文档/Xponge文档/Xponge/tools/unittests/test_5_nopbc)


### [Xponge.tools.unittests.test_5_rerun](/文档/Xponge文档/Xponge/tools/unittests/test_5_rerun)


### [Xponge.tools.unittests.test_5_wall](/文档/Xponge文档/Xponge/tools/unittests/test_5_wall)


### [Xponge.tools.unittests.test_5_wrap](/文档/Xponge文档/Xponge/tools/unittests/test_5_wrap)


### [Xponge.tools.unittests.test_6_andersen_barostat](/文档/Xponge文档/Xponge/tools/unittests/test_6_andersen_barostat)


### [Xponge.tools.unittests.test_6_andersen_thermostat](/文档/Xponge文档/Xponge/tools/unittests/test_6_andersen_thermostat)


### [Xponge.tools.unittests.test_6_berendsen_barostat](/文档/Xponge文档/Xponge/tools/unittests/test_6_berendsen_barostat)


### [Xponge.tools.unittests.test_6_berendsen_thermostat](/文档/Xponge文档/Xponge/tools/unittests/test_6_berendsen_thermostat)


### [Xponge.tools.unittests.test_6_bussi_barostat](/文档/Xponge文档/Xponge/tools/unittests/test_6_bussi_barostat)


### [Xponge.tools.unittests.test_6_bussi_thermostat](/文档/Xponge文档/Xponge/tools/unittests/test_6_bussi_thermostat)


### [Xponge.tools.unittests.test_6_langevin](/文档/Xponge文档/Xponge/tools/unittests/test_6_langevin)


### [Xponge.tools.unittests.test_6_middle_langevin](/文档/Xponge文档/Xponge/tools/unittests/test_6_middle_langevin)


### [Xponge.tools.unittests.test_6_monte_carlo_barostat](/文档/Xponge文档/Xponge/tools/unittests/test_6_monte_carlo_barostat)


### [Xponge.tools.unittests.test_6_nose_hoover_chain](/文档/Xponge文档/Xponge/tools/unittests/test_6_nose_hoover_chain)


### [Xponge.tools.unittests.test_6_rdf](/文档/Xponge文档/Xponge/tools/unittests/test_6_rdf)


### [Xponge.tools.unittests.test_7_msd](/文档/Xponge文档/Xponge/tools/unittests/test_7_msd)


### [Xponge.tools.unittests.test_7_tip4p](/文档/Xponge文档/Xponge/tools/unittests/test_7_tip4p)


### [Xponge.tools.unittests.test_8_meta1d](/文档/Xponge文档/Xponge/tools/unittests/test_8_meta1d)


### [Xponge.tools.unittests.test_8_sits](/文档/Xponge文档/Xponge/tools/unittests/test_8_sits)


### [Xponge.tools.unittests.test_8_tmd](/文档/Xponge文档/Xponge/tools/unittests/test_8_tmd)


### [Xponge.tools.unittests.test_8_umbrella](/文档/Xponge文档/Xponge/tools/unittests/test_8_umbrella)


### [Xponge.tools.unittests.test_9_fep](/文档/Xponge文档/Xponge/tools/unittests/test_9_fep)


## functions

## classes

### XpongeTestRunner

the unittest wrapper of Xponge tests

