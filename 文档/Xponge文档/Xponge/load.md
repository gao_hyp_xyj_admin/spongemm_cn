# Xponge.load

This **module** is used to load and read

## subpackages


## submodules


## functions

### load_mol2

This **function** is used to load a mol2 file


parameters | explanation
--- | ---
file | the name of the input file or an instance of io.IOBase
ignore_atom_type | ignore the atom types in the mol2 file
as_template | only read the mol2 file as some residue types and no molecule will created


return | a Molecule instance if as_template is False
---|---

### load_coordinate

This **function** is used to read the SPONGE coordinate in file


parameters | explanation
--- | ---
filename | the coordinate file to load
mol | the molecule or residue to load the coordinate into


return | two numpy arrays, representing the coordinates and the box information respectively
---|---

### load_frcmod

This **function** is used to load a frcmod file


parameters | explanation
--- | ---
filename | the name of the file to load
nbtype | the non-bonded interaction recording type in the frcmod file.


return | a list of strings, including atoms, bonds, angles, propers, impropers, ljs, cmap information respectively
---|---

### load_parmdat

This **function** is used to load a parmdat file


parameters | explanation
--- | ---
filename | the name of the file to load


return | a list of strings, including atoms, bonds, angles, propers, impropers, ljs, nb14s information respectively
---|---

### load_rst7

This **function** is used to load a rst7 file


parameters | explanation
--- | ---
filename | the name of the file to load
mol | the molecule to load the coordinates


return | a tuple including coordinates and box information
---|---

### load_molitp

This **function** is used to load a molitp file


parameters | explanation
--- | ---
filename | the name of the file to load
water_replace | whether to change water to SPONGE. True as default.
head_prefix | a string, the prefix will be added to the name of the first residue of each molecule
tail_prefix | a string, the prefix will be added to the name of the last residue of each molecule
macros | the macros used to read the Gromacs topology file


return | 1. an Xponge.Molecule representing the systema. None if not define
---|---

### load_gro

This **function** is used to read the GROMACS coordinate file


parameters | explanation
--- | ---
filename | the coordinate file to load
mol | the molecule or residue to load the coordinate into


return | two numpy arrays, representing the coordinates and the box information respectively
---|---

## classes

### GromacsTopologyIterator

This **class** is used to read a GROMACS topology file


parameters | explanation
--- | ---
filename | the name of the file to read
macros | the macros used to read the Gromacs topology file

