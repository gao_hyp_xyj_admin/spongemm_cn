# Xponge.assign.tpacm4

This **package** is used to calculate the TPACM4 (Transferable Partial Atomic Charge Model upto 4 bonds) charge.

## subpackages


## submodules


## functions

### tpacm4

This **function** is used to calculate the Gasteiger charge of an assignment


parameters | explanation
--- | ---
assign | the Assign instance
charge | the total charge of the Assignment


return | a list of charges
---|---

