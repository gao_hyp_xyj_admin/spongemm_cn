# Xponge.assign.phmodel

This **module** helps to assign hydrogens according to pH

## subpackages


## submodules


## functions

## classes

### PHModelAssignment

This **class** includes the functions to assign the pH model


parameters | explanation
--- | ---
assign | the father Assignment instance
ph | the pH value

#### main

This **function** is the main function to do the pH model assignment


return | the sum of the formal charge now
---|---

