# Xponge.assign.bond_order

This **module** helps to assign bond orders

## subpackages


## submodules


## functions

## classes

### BondOrderAssignment

This **class** includes the functions to assign bond orders


parameters | explanation
--- | ---
original_penalties | the original penalties dict
max_stat | the max valence stats to iterate
assign | the father Assignment instance
total_charge | the total charge of the molecule
extra_criteria | a function as the extra convergence criteria. The function will receive the assignment as input, and give True or False as output.

#### main

This **function** is the main function to do the bond order assignment


return | True for success, False for failure
---|---

