# Xponge.assign.gasteiger

This **module** is used to calculate the Gasteiger charge.

## subpackages


## submodules


## functions

### gasteiger

This **function** is used to calculate the Gasteiger charge of an assignment


parameters | explanation
--- | ---
assign | the Assign instance


return | a list of charges
---|---

