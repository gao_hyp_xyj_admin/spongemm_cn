# Xponge.assign

This **package** is used to assign the properties for atoms, residues and molecules

## subpackages


### [Xponge.assign.tpacm4](/文档/Xponge文档/Xponge/assign/tpacm4)


## submodules


### [Xponge.assign.bond_order](/文档/Xponge文档/Xponge/assign/bond_order)


### [Xponge.assign.gasteiger](/文档/Xponge文档/Xponge/assign/gasteiger)


### [Xponge.assign.phmodel](/文档/Xponge文档/Xponge/assign/phmodel)


### [Xponge.assign.resp](/文档/Xponge文档/Xponge/assign/resp)


## functions

### get_assignment_from_smiles

This **function** gets an Assign instance from smiles


parameters | explanation
--- | ---
smiles | the smiles to get


return | the Assign instance
---|---

### get_assignment_from_pdb

This **function** gets an Assign instance from a pdb file


parameters | explanation
--- | ---
file | the name of the input file or an instance of io.IOBase
only_residue | only get the residue with the name same as ``only_residue``
bond_tolerance | the parameter to determine the atomic connections. The larger tolerance, the easier to set a bond between two atoms
total_charge | the total charge of the molecule used when aligning bond orders. If None is given, the total charge will not be checked


return | the Assign instance
---|---

### get_assignment_from_residuetype

This **function** gets an Assign instance from a ResidueType instance


parameters | explanation
--- | ---
restype | the ResidueType instance


return | the Assign instance
---|---

### get_assignment_from_xyz

This **function** gets an Assign instance from a xyz file


parameters | explanation
--- | ---
file | the name of the input file or an instance of io.IOBase
bond_tolerance | the parameter to determine the atomic connections. The larger tolerance, the easier to set a bond between two atoms
total_charge | the total charge of the molecule used when aligning bond orders. If None is given, the total charge will not be checked


return | the Assign instance
---|---

### get_assignment_from_mol2

This **function** gets an Assign instance from a mol2 file


parameters | explanation
--- | ---
file | the name of the input file or an instance of io.IOBase
total_charge | the total charge of the molecule used when aligning bond orders. If "sum" is given, the sum of the partial charges will be used; If None is given, the total charge will not be checked


return | the Assign instance
---|---

### get_assignment_from_cif

This **function** gets an Assign instance and a preprocessed lattice information from a cif file


parameters | explanation
--- | ---
file | the name of the input file or an instance of io.IOBase
total_charge | the total charge of the molecule used when aligning bond orders. 0 for default.
orthogonal_threshold | cell angle with the difference less than this parameter will be considered to be orthogonal (in degree, and 5 for default)


return | the Assign instance and a dict which stores the preprocessed lattice information
---|---

## classes

### AssignRule

This **class** is to be the rule to determine the atom type for one atom


parameters | explanation
--- | ---
name | the name of the rule

#### add_rule

This **function** is used as a **decorator** to add the atom type - judge function


parameters | explanation
--- | ---
atomtype | a string or an AtomType instance
priority | if more than one judge function returns True,  the atom type with higher priority will be chosen. If the priority levels of the functions are the same, the atom type which is added first will be chosen.


return | a **function**, which wraps a judge function (receiving the Assign instance and the atom index and giving True or False as a result)
---|---

#### add_rings_basic_marker




parameters | explanation
--- | ---
assign | 
rings | 


return | None
---|---

#### check_rings_type




parameters | explanation
--- | ---
assign | 
rings | 


return | None
---|---

#### get_rings




parameters | explanation
--- | ---
assign | 


return | None
---|---

#### get_3_neighbors




return | None
---|---

#### check_pure_aromatic




parameters | explanation
--- | ---
assign | 


return | None
---|---

#### check_pure_aliphatic_and_planar




parameters | explanation
--- | ---
assign | 


return | None
---|---

#### check_out_plane_double_bond




parameters | explanation
--- | ---
assign | 


return | None
---|---

#### check_aromatic




parameters | explanation
--- | ---
assign | 


return | None
---|---

### Assign

This **class** is used to assign properties for atoms, which is called an "assignment"


parameters | explanation
--- | ---
name | the name of the molecule

#### add_index_to_name

This **function** renames the atoms by adding the index to the element name


return | None
---|---

#### atom_judge

This **function** helps judge whether the atom belongs to the mask. For example, "O2" means an oxygen atom connected to two other atoms, "N4" means a nitrogen atom connected to four other atoms.


parameters | explanation
--- | ---
atom | the index of the atom
string | a string mask  of a list of string masks.


return | None
---|---

#### add_atom

This **function** adds an atom to the Assign


parameters | explanation
--- | ---
element | the chemical symbol for the element. "O" - oxygen, "H" - hydrogen for example.
x | the x coordinate
y | the y coordinate
z | the z coordinate
name | the name of the atom
charge | the charge of the atom


return | None
---|---

#### add_atom_marker

This **function** adds a marker to an atom


parameters | explanation
--- | ---
atom | the atom index
marker | the marker


return | None
---|---

#### add_bond

This **function** adds a bond to two atoms


parameters | explanation
--- | ---
atom1 | the index of the first atom
atom2 | the index of the the second atom
order | the bond order


return | None
---|---

#### add_bond_marker

This **function** adds a marker to a bond


parameters | explanation
--- | ---
atom1 | the index of the first atom
atom2 | the index of the the second atom
marker | the marker
only1 | only add the marker to the atom1 - atom2 bond instead of the atom2 - atom1 bond


return | None
---|---

#### delete_atom

This **function** deletes the atom


parameters | explanation
--- | ---
atom | the index of the atom to delete


return | None
---|---

#### delete_bond

This **function** deletes the bond between two atoms


parameters | explanation
--- | ---
atom1 | the index of the first atom
atom2 | the index of the the second atom


return | None
---|---

#### determine_equal_atoms

This **function** dertermines the chemical equalvalent atoms


return | a list of equalvalent atom index lists
---|---

#### determine_ring_and_bond_type

This **function** determine the ring and the bond type


return | None
---|---

#### determine_atom_type

This **function** determines the atom type.


parameters | explanation
--- | ---
rule | a string or an AssignRule instance


return | if the attribute "pure_string" of the rule is False, the atom types will be saved inplace and return
---|---

#### determine_connectivity

This **function** determines the connectivity based on atomic distances


parameters | explanation
--- | ---
simple_cutoff | the distance cutoff to determine whether the two atoms are connected. If None (default),
tolerance | the tolerance factor for the default method


return | None
---|---

#### check_connectivity

This **function** checks whether all atoms are connected in one graph


return | True or False
---|---

#### to_residuetype

This **function** converts the Assign instance to the ResidueType instance


parameters | explanation
--- | ---
name | the name of the ResidueType instance
charge | the charge of atoms. If set to None, internal charge will be used


return | the ResidueType instance
---|---

#### calculate_charge

This **function** calculates the partial charge for every atom.


parameters | explanation
--- | ---
method | the method to calculate the charge
parameters | the parameters to calculate the charge


return | None
---|---

#### kekulize

This **function** kekulizes the structure.


return | None
---|---

#### uff_optimize

This **function** uses rdkit and uff to optimize the structure

#### save_as_pdb

This **function** saves the instance as a pdb file


parameters | explanation
--- | ---
filename | the name of the output file


return | None
---|---

#### save_as_mol2

This **function** saves the instance as a mol2 file


parameters | explanation
--- | ---
filename | the name of the output file
atomtype | the rule of atom types.


return | None
---|---

#### set_ph

This **function** sets the pH value, and adds or deletes the related hydrogens.


parameters | explanation
--- | ---
ph | the pH value


return | the sum of final formal charge
---|---

