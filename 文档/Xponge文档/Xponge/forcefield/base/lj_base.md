# Xponge.forcefield.base.lj_base

This **module** is the basic setting for the force field property of Lennard-Jones

## subpackages


## submodules


## functions

### lorentz_berthelot_for_a

This **function** is used to calculate the A coefficient for Lorentz_Berthelot mix rule


parameters | explanation
--- | ---
epsilon1 | the epsilon parameter of the first atom
rmin1 | the rmin parameter of the first atom
epsilon2 | the epsilon parameter of the second atom
rmin2 | the rmin parameter of the second atom


return | the A coefficient for the atom pair
---|---

### lorentz_berthelot_for_b

This **function** is used to calculate the B coefficient for Lorentz_Berthelot mix rule


parameters | explanation
--- | ---
epsilon1 | the epsilon parameter of the first atom
rmin1 | the rmin parameter of the first atom
epsilon2 | the epsilon parameter of the second atom
rmin2 | the rmin parameter of the second atom


return | the B coefficient for the atom pair
---|---

### good_hope_for_a

This **function** is used to calculate the A coefficient for Good-Hope mix rule


parameters | explanation
--- | ---
epsilon1 | the epsilon parameter of the first atom
rmin1 | the rmin parameter of the first atom
epsilon2 | the epsilon parameter of the second atom
rmin2 | the rmin parameter of the second atom


return | the A coefficient for the atom pair
---|---

### good_hope_for_b

This **function** is used to calculate the B coefficient for Good-Hope mix rule


parameters | explanation
--- | ---
epsilon1 | the epsilon parameter of the first atom
rmin1 | the rmin parameter of the first atom
epsilon2 | the epsilon parameter of the second atom
rmin2 | the rmin parameter of the second atom


return | the B coefficient for the atom pair
---|---

### write_lj

This **function** is used to write SPONGE input file


parameters | explanation
--- | ---
self | the Molecule instance


return | the string to write
---|---

### lj_todo

This **function** is used to get MindSponge system and energy


parameters | explanation
--- | ---
self | the Molecule instance
sys_kwarg | a dict, the kwarg for system
ene_kwarg | a dict, the kwarg for force field


return | None
---|---

