# Xponge.forcefield.base.listed_force_base

This **module** is the basic setting for the force field property of general listed forces

## subpackages


## submodules


## functions

### write_listed_forces

This **function** is used to write SPONGE input file


parameters | explanation
--- | ---
self | the Molecule instance


return | the string to write
---|---

