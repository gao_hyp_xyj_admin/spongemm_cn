# Xponge.forcefield.base.nb14_extra_base

This **module** is the basic setting for the force field format of 3-parameter non bonded 1-4 interactions

## subpackages


## submodules


## functions

### get_nb14_extra_lj

This **function** is used to get the LJ parameters for the extra nb14 interactions


parameters | explanation
--- | ---
atom1 | the Atom instance
atom2 | the Atom instance


return | the A and B coefficients of LJ
---|---

### exclude_to_nb14_extra

This **function** is used to calculate nb14_extra instead of non-bonded interactions for atom1 and atom2


parameters | explanation
--- | ---
molecule | the Molecule instance
atom1 | the Atom instance
atom2 | the Atom instance


return | None
---|---

### nb14_to_nb14_extra

This **function** is used to convert nb14 to nb14_extra


parameters | explanation
--- | ---
molecule | the Molecule instance


return | None
---|---

### write_nb14

This **function** is used to write SPONGE input file


parameters | explanation
--- | ---
self | the Molecule instance


return | the string to write
---|---

