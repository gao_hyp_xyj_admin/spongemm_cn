# Xponge.forcefield.base.virtual_atom_base

This **module** is the basic setting for the force field format of virtual atoms

## subpackages


## submodules


## functions

### write_virtual_atoms

This **function** is used to write SPONGE input file


parameters | explanation
--- | ---
self | the Molecule instance


return | the string to write
---|---

