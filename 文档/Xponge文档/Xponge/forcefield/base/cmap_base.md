# Xponge.forcefield.base.cmap_base

This **module** is the basic setting for the force field format of atom-specific cmap

## subpackages


## submodules


## functions

### cmap_same_force

This **function** is used to return the same force type for an atom list


parameters | explanation
--- | ---
_ | 
atom_list | 


return | None
---|---

### write_cmap

This **function** is used to write SPONGE input file


parameters | explanation
--- | ---
self | the Molecule instance


return | the string to write
---|---

