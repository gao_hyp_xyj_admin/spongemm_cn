# Xponge.forcefield.base.mass_base

This **module** is the basic setting for the force field property of mass

## subpackages


## submodules


## functions

### write_mass

This **function** is used to write SPONGE input file


parameters | explanation
--- | ---
self | the Molecule instance


return | the string to write
---|---

