# Xponge.forcefield.base.charge_base

This **module** is the basic setting for the force field property of charge

## subpackages


## submodules


## functions

### write_charge

This **function** is used to write SPONGE input file


parameters | explanation
--- | ---
self | the Molecule instance


return | the string to write
---|---

