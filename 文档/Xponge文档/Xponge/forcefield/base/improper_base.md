# Xponge.forcefield.base.improper_base

This **module** is the basic setting for the force field format of periodic proper and improper dihedral

## subpackages


## submodules


## functions

### improper_same_force

This **function** is used to return the same force type for an atom list


parameters | explanation
--- | ---
_ | 
atom_list | 


return | None
---|---

### write_dihedral

This **function** is used to write SPONGE input file


parameters | explanation
--- | ---
self | the Molecule instance


return | the string to write
---|---

