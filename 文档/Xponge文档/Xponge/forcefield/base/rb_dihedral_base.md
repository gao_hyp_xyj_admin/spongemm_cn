# Xponge.forcefield.base.rb_dihedral_base

This **module** is the basic setting for the force field format of Ryckaert-Bellemans dihedral

## subpackages


## submodules


## functions

### write_dihedral

This **function** is used to write SPONGE input file


parameters | explanation
--- | ---
self | the Molecule instance


return | the string to write
---|---

