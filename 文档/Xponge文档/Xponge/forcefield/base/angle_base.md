# Xponge.forcefield.base.angle_base

This **module** is the basic setting for the force field format of harmonic angle

## subpackages


## submodules


## functions

### write_angle

This **function** is used to write SPONGE input file


parameters | explanation
--- | ---
self | the Molecule instance


return | the string to write
---|---

