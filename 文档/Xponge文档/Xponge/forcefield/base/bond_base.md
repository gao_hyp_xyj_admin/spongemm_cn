# Xponge.forcefield.base.bond_base

This **module** is the basic setting for the force field format of harmonic bond

## subpackages


## submodules


## functions

### write_bond

This **function** is used to write SPONGE input file


parameters | explanation
--- | ---
self | the Molecule instance


return | the string to write
---|---

