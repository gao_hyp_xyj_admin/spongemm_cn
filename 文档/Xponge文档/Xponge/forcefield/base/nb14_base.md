# Xponge.forcefield.base.nb14_base

This **module** is the basic setting for the force field format of 2-parameter non bonded 1-4 interactions

## subpackages


## submodules


## functions

### write_nb14

This **function** is used to write SPONGE input file


parameters | explanation
--- | ---
self | the Molecule instance


return | the string to write
---|---

