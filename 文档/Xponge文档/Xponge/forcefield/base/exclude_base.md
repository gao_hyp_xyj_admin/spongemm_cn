# Xponge.forcefield.base.exclude_base

This **module** is the basic setting for the force field non bonded exclusion

## subpackages


## submodules


## functions

## classes

### Exclude

This **class** is used to set non bonded exclusion generally

#### get_excluded_atoms

This **function** gives the excluded atoms of a molecule


parameters | explanation
--- | ---
molecule | a Molecule instance


return | a dict, which stores the atom - excluded atoms mapping
---|---

