# Xponge.forcefield.charmm.charmm36

This **package** sets the basic configuration of charmm36-july2022 force field

## subpackages


## submodules


### [Xponge.forcefield.charmm.charmm36.dna](/文档/Xponge文档/Xponge/forcefield/charmm/charmm36/dna)


### [Xponge.forcefield.charmm.charmm36.protein](/文档/Xponge文档/Xponge/forcefield/charmm/charmm36/protein)


### [Xponge.forcefield.charmm.charmm36.rna](/文档/Xponge文档/Xponge/forcefield/charmm/charmm36/rna)


## functions

