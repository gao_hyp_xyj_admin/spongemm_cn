# Xponge.forcefield.special.fep

This **module** gives the basic functions for fep calculations

## subpackages


## submodules


## functions

### save_hard_core_lj

This **function** is used to save hard core lj


return | None
---|---

### save_soft_core_lj

This **function** is used to save soft core lj


return | None
---|---

### write_soft_lj




parameters | explanation
--- | ---
self | 


return | None
---|---

### intramolecule_nb_to_nb14

This **function** convert the non bonded interactions to nb14 interactions within the molecule


parameters | explanation
--- | ---
mol_a | the Molecule instance
perturbating_residues | the residue(s) to be perturbed


return | None
---|---

### get_free_molecule

This **function** makes the molecule to be "free", having no interaction with other molecules


parameters | explanation
--- | ---
mol_a | the Molecule instance
perturbating_residues | the residues to be perturbed
intra_fep | whether clear intramolecular non bonded interactions


return | a new Molecule instance
---|---

### merge_force_field

This **function** merges one molecule in two different force fields (two Molecule instances) into one


parameters | explanation
--- | ---
mol_a | the Molecule instance in the initial lambda stat
mol_b | the Molecule instance in the final lambda stat
default_lambda | the lambda to scale the force if no ``specific_lambda`` is set for the force
specific_lambda | a dict to map the force to its special scale factor lambda
intra_fep | whether clear intramolecular non bonded interactions


return | the Molecule instance merged
---|---

