# Xponge.forcefield.special.min

This **module** is used to save parameters for special minimization

## subpackages


## submodules


## functions

### write_zero_mass_for_hydrogen

This **function** sets the mass of hydrogen and no lj atoms to zero, to freeze them


parameters | explanation
--- | ---
self | the Molecule instance


return | the string to write
---|---

### write_zero_lj

This **function** sets all the lj parameters to zero


parameters | explanation
--- | ---
self | the Molecule instance


return | the string to write
---|---

### write_zero_charge

This **function** sets all the charge parameters to zero


parameters | explanation
--- | ---
self | the Molecule instance


return | the string to write
---|---

### save_min_bonded_parameters

This **function** saves parameters to only minimize bonded force when saving SPONGE inputs


return | None
---|---

### do_not_save_min_bonded_parameters

This **function** does not save parameters to only minimize bonded force when saving SPONGE inputs


return | None
---|---

