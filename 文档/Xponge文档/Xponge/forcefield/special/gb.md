# Xponge.forcefield.special.gb

This **module** set the generalized Born force field

## subpackages


## submodules


## functions

### bondi_radii

This **function** receives an atom and sets the Bondi radii


parameters | explanation
--- | ---
atom | the Atom instance


return | None
---|---

### modified_bondi_radii

This **function** receives an atom and sets the modified Bondi radii


parameters | explanation
--- | ---
atom | the Atom instance


return | None
---|---

### write_gb_radii_and_scaler

This **function** is used to write gb radii and scaler when saving SPONGE inputs


parameters | explanation
--- | ---
self | the Molecule instance


return | the string to write
---|---

### set_gb_radius

This **function** is used to set GB radius for the molecule


parameters | explanation
--- | ---
mol | the molecule, either a Molecule instance, a ResidueType instance or a Residue instance
radius_set | a function, which receives an Atom instance and sets the radius


return | None
---|---

