# Xponge.forcefield.amber.glycam_06j

This **package** sets the basic configuration for GLYCAM-06j

## subpackages


## submodules


### [Xponge.forcefield.amber.glycam_06j.d_furanose](/文档/Xponge文档/Xponge/forcefield/amber/glycam_06j/d_furanose)


### [Xponge.forcefield.amber.glycam_06j.d_pyranose](/文档/Xponge文档/Xponge/forcefield/amber/glycam_06j/d_pyranose)


### [Xponge.forcefield.amber.glycam_06j.glycoprotein](/文档/Xponge文档/Xponge/forcefield/amber/glycam_06j/glycoprotein)


### [Xponge.forcefield.amber.glycam_06j.l_furanose](/文档/Xponge文档/Xponge/forcefield/amber/glycam_06j/l_furanose)


### [Xponge.forcefield.amber.glycam_06j.l_pyranose](/文档/Xponge文档/Xponge/forcefield/amber/glycam_06j/l_pyranose)


## functions

### set_head

This **function** sets the head of the carbohydrate residue type to the n-th oxygen atom


parameters | explanation
--- | ---
res | 
n | 


return | None
---|---

