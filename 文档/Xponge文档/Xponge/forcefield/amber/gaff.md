# Xponge.forcefield.amber.gaff

This **module** sets the basic configuration for gaff

## subpackages


## submodules


## functions

### parmchk2_gaff

This **function** is to do parmchk2 for gaff


parameters | explanation
--- | ---
ifname | a string of mol2 file name, a ResidueType, Residue or Molecule instance
ofname | the output file name
direct_load | directly load the output file after writing it
keep | do not delete the output file after loading it


return | None
---|---

