# Xponge.forcefield.charmm

This **package** sets the basic configuration of CHARMM force field

## subpackages


### [Xponge.forcefield.charmm.charmm27](/文档/Xponge文档/Xponge/forcefield/charmm/charmm27)


### [Xponge.forcefield.charmm.charmm36](/文档/Xponge文档/Xponge/forcefield/charmm/charmm36)


## submodules


### [Xponge.forcefield.charmm.tip3p](/文档/Xponge文档/Xponge/forcefield/charmm/tip3p)


### [Xponge.forcefield.charmm.tip3p_charmm](/文档/Xponge文档/Xponge/forcefield/charmm/tip3p_charmm)


## functions

### load_parameter_from_ffitp

This **function** is used to get charmm force field parameters from GROMACS ffitp


parameters | explanation
--- | ---
filename | the name of the input file
prefix | the folder of the file


return | None
---|---

