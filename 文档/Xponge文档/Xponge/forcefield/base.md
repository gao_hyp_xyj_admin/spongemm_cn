# Xponge.forcefield.base



## subpackages


## submodules


### [Xponge.forcefield.base.angle_base](/文档/Xponge文档/Xponge/forcefield/base/angle_base)


### [Xponge.forcefield.base.bond_base](/文档/Xponge文档/Xponge/forcefield/base/bond_base)


### [Xponge.forcefield.base.charge_base](/文档/Xponge文档/Xponge/forcefield/base/charge_base)


### [Xponge.forcefield.base.cmap_base](/文档/Xponge文档/Xponge/forcefield/base/cmap_base)


### [Xponge.forcefield.base.dihedral_base](/文档/Xponge文档/Xponge/forcefield/base/dihedral_base)


### [Xponge.forcefield.base.exclude_base](/文档/Xponge文档/Xponge/forcefield/base/exclude_base)


### [Xponge.forcefield.base.improper_base](/文档/Xponge文档/Xponge/forcefield/base/improper_base)


### [Xponge.forcefield.base.listed_force_base](/文档/Xponge文档/Xponge/forcefield/base/listed_force_base)


### [Xponge.forcefield.base.lj_base](/文档/Xponge文档/Xponge/forcefield/base/lj_base)


### [Xponge.forcefield.base.mass_base](/文档/Xponge文档/Xponge/forcefield/base/mass_base)


### [Xponge.forcefield.base.nb14_base](/文档/Xponge文档/Xponge/forcefield/base/nb14_base)


### [Xponge.forcefield.base.nb14_extra_base](/文档/Xponge文档/Xponge/forcefield/base/nb14_extra_base)


### [Xponge.forcefield.base.rb_dihedral_base](/文档/Xponge文档/Xponge/forcefield/base/rb_dihedral_base)


### [Xponge.forcefield.base.soft_bond_base](/文档/Xponge文档/Xponge/forcefield/base/soft_bond_base)


### [Xponge.forcefield.base.ub_angle_base](/文档/Xponge文档/Xponge/forcefield/base/ub_angle_base)


### [Xponge.forcefield.base.virtual_atom_base](/文档/Xponge文档/Xponge/forcefield/base/virtual_atom_base)


## functions

