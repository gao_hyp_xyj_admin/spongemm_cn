# Xponge.forcefield.opls

This **package** sets the basic configuration of OPLS force field

## subpackages


### [Xponge.forcefield.opls.oplsaam](/文档/Xponge文档/Xponge/forcefield/opls/oplsaam)


## submodules


### [Xponge.forcefield.opls.tip4p](/文档/Xponge文档/Xponge/forcefield/opls/tip4p)


## functions

### load_parameter_from_ffitp

This **function** is used to get opls force field parameters from GROMACS ffitp


parameters | explanation
--- | ---
filename | the name of the input file
prefix | the folder of the file


return | None
---|---

