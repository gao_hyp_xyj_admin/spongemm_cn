# Xponge.forcefield.amber

This **package** sets the basic configuration of amber force field

## subpackages


### [Xponge.forcefield.amber.glycam_06j](/文档/Xponge文档/Xponge/forcefield/amber/glycam_06j)


## submodules


### [Xponge.forcefield.amber.bsc1](/文档/Xponge文档/Xponge/forcefield/amber/bsc1)


### [Xponge.forcefield.amber.ff14sb](/文档/Xponge文档/Xponge/forcefield/amber/ff14sb)


### [Xponge.forcefield.amber.ff19sb](/文档/Xponge文档/Xponge/forcefield/amber/ff19sb)


### [Xponge.forcefield.amber.gaff](/文档/Xponge文档/Xponge/forcefield/amber/gaff)


### [Xponge.forcefield.amber.lipid14](/文档/Xponge文档/Xponge/forcefield/amber/lipid14)


### [Xponge.forcefield.amber.lipid17](/文档/Xponge文档/Xponge/forcefield/amber/lipid17)


### [Xponge.forcefield.amber.ol15](/文档/Xponge文档/Xponge/forcefield/amber/ol15)


### [Xponge.forcefield.amber.ol3](/文档/Xponge文档/Xponge/forcefield/amber/ol3)


### [Xponge.forcefield.amber.opc](/文档/Xponge文档/Xponge/forcefield/amber/opc)


### [Xponge.forcefield.amber.rsff2c](/文档/Xponge文档/Xponge/forcefield/amber/rsff2c)


### [Xponge.forcefield.amber.spce](/文档/Xponge文档/Xponge/forcefield/amber/spce)


### [Xponge.forcefield.amber.tip3p](/文档/Xponge文档/Xponge/forcefield/amber/tip3p)


### [Xponge.forcefield.amber.tip4p](/文档/Xponge文档/Xponge/forcefield/amber/tip4p)


### [Xponge.forcefield.amber.tip4pew](/文档/Xponge文档/Xponge/forcefield/amber/tip4pew)


## functions

### load_parameters_from_parmdat

This **function** is used to get amber force field parameters from parmdat files


parameters | explanation
--- | ---
filename | the name of the input file
prefix | whether add the AMBER_DATA_DIR to the filename


return | None
---|---

### load_parameters_from_frcmod

This **function** is used to get amber force field parameters from frcmod files


parameters | explanation
--- | ---
filename | the name of the input file
include_cmap | whether include cmap
prefix | whether add the AMBER_DATA_DIR to the filename


return | None
---|---

### cmap_same_force

This **function** is used to return the same force type for an atom list


parameters | explanation
--- | ---
_ | 
atom_list | 


return | None
---|---

