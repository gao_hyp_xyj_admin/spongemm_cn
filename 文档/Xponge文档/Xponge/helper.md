# Xponge.helper

This **module** is used to provide help functions and classes

## subpackages


## submodules


### [Xponge.helper.cv](/文档/Xponge文档/Xponge/helper/cv)


### [Xponge.helper.file](/文档/Xponge文档/Xponge/helper/file)


### [Xponge.helper.gromacs](/文档/Xponge文档/Xponge/helper/gromacs)


### [Xponge.helper.math](/文档/Xponge文档/Xponge/helper/math)


### [Xponge.helper.namespace](/文档/Xponge文档/Xponge/helper/namespace)


### [Xponge.helper.rdkit](/文档/Xponge文档/Xponge/helper/rdkit)


## functions

### xprint

This **function** is used to print some contents according to the verbose level in GlobalSetting


parameters | explanation
--- | ---
to_print | the contents to print
verbose | only print when the verbose level is not less than this value


return | None
---|---

### debug

This **function** sets the mode to debug or not


parameters | explanation
--- | ---
mode | the value to set the debug mode

### set_unit_transfer_function

This **function** is used to return a function to add a static method  ``_unit_transfer`` for a class.


parameters | explanation
--- | ---
sometype | the class


return | the **decorator**
---|---

### add_unit_transfer_function

This **function** is used to return a function to add a static method  ``_unit_transfer`` for a class.


parameters | explanation
--- | ---
sometype | the class


return | the **decorator**
---|---

### verbose

the gloabl verbose level

### add_pdb_residue_name_mapping

This **function** is used to add the residue name mapping to the property ``PDBResidueNameMap``


parameters | explanation
--- | ---
place | head or tail
pdb_name | the residue name in pdb
real_name | the residue name in Python


return | None
---|---

### set_invisible_bonded_forces

This **function** is used to disables the types of bonded forces when building.


parameters | explanation
--- | ---
types | the types to set


return | None
---|---

### set_visible_bonded_forces

This **function** is used to disables the types of bonded forces except named here when building.


parameters | explanation
--- | ---
types | the types to set


return | None
---|---

## classes

### Xdict

This **class** is used to be a dict which can give not_found_message


parameters | explanation
--- | ---
not_found_method | the method (function) which will accept the key and return the value when the key is not found. **New From 1.2.6.7**
not_found_message | the string to print when the key is not found

### ReasonedBool

This **class** is a boolean value with a reason


parameters | explanation
--- | ---
value | True or False
reason | the reason why the boolean value is given

### Type

This **class** is the abstract class of the types (atom types, bonded force types and so on).


parameters | explanation
--- | ---
name | the name of the type
kwargs | parameters of the type

#### add_property

This **function** is used to add a property to the class


parameters | explanation
--- | ---
parm_fmt | a dict mapping the name and the format of the property
parm_default | a dict mapping the name and the default value of the property


return | None
---|---

#### set_property_unit

This **function** is used to set the unit of the property of the class


parameters | explanation
--- | ---
prop | the name of the property
unit_type | the type of the unit
base_unit | the basic unit used in Python


return | None
---|---

#### new_from_string

This **function** is used to update the types of the class


parameters | explanation
--- | ---
string | the string to update
skip_lines | skip the first ``skip_lines`` line(s)


return | None
---|---

#### new_from_file

This **function** is used to update the types of the class


parameters | explanation
--- | ---
filename | the name of the file
skip_lines | skip the first ``skip_lines`` line(s)


return | None
---|---

#### new_from_dict

This **function** is used to update the types of the class


parameters | explanation
--- | ---
dic | the dict of the parameters


return | None
---|---

#### get_class_name

This **function** gives the ._name of the class


return | the name of the class
---|---

#### clear_type

This **function** clears the instance(s) of the class


parameters | explanation
--- | ---
name | the instance name to clear. If None, all instances will be cleared


return | None
---|---

#### set_type

This **function** sets the instance into the class


parameters | explanation
--- | ---
name | the instance name
toset | the instance to set


return | None
---|---

#### get_type

This **function** gets the instance of the class


parameters | explanation
--- | ---
name | the instance name


return | the instance to set
---|---

#### get_all_types

This **function** gets the all instances of the class


return | a dict mapping the name and the instance
---|---

#### update

This **function** is used to update the properties of the instance


parameters | explanation
--- | ---
kwargs | parameters to update


return | None
---|---

### AbstractMolecule

This abstract **class** is used to judge whether a class can be treated as a molecule

### AtomType

This **class** is a subclass of Type, for atom types


parameters | explanation
--- | ---
name | the name of the type
kwargs | parameters of the type

### ResidueType

This **class** is a subclass of Type, for residue types


parameters | explanation
--- | ---
name | the name of the type
kwargs | parameters of the type

#### head

the name of the first atom in the head

#### tail

the name of the first atom in the tail

#### head_next

the name of the second atom in the head

#### tail_next

the name of the second atom in the tail

#### head_length

the length of the bond connected to the last residue

#### tail_length

the length of the bond connected to the next residue

#### head_link_conditions

the link conditions to the last residue

#### tail_link_conditions

the link conditions to the next residue

#### name2atom

This **function** convert an atom name to an Atom instance


parameters | explanation
--- | ---
name | the name


return | the Atom instance
---|---

#### atom2index

This **function** convert an Atom instance to its index


parameters | explanation
--- | ---
atom | the Atom instance


return | the index
---|---

#### atom2name

This **function** convert an Atom instance to its name


parameters | explanation
--- | ---
atom | the Atom instance


return | the name
---|---

#### name2index

This **function** convert an atom name to its index


parameters | explanation
--- | ---
name | the name


return | the index
---|---

#### add_atom

This **function** is used to add an atom to the residue type.


parameters | explanation
--- | ---
name | the name of the atom
atom_type | the type of the atom
x | the coordinate x
y | the coordinate y
z | the coordinate z


return | None
---|---

#### omit_atoms

This **function** omits some atoms from the ResidueType


parameters | explanation
--- | ---
atoms | the atom(s) to omit
charge | the total charge of the residue type after the omission. None to use the charge sum of the unomitted atoms


return | None
---|---

#### add_connectivity

This **function** is used to add the connectivity between two atoms to the residue type.


parameters | explanation
--- | ---
atom0 | the atom name or the Atom instance
atom1 | the atom name or the Atom instance


return | None
---|---

#### remove_connectivity

This **function** is used to remove the connectivity between two atoms from the residue type.


parameters | explanation
--- | ---
atom0 | the atom name or the Atom instance
atom1 | the atom name or the Atom instance


return | None
---|---

#### remove_periodic_connectivity

This **function** is used to remove the connectivity between atoms larger than the given cutoff


parameters | explanation
--- | ---
cutoff | the cutoff distance to recognize the connectivity as periodic or unperiodic


return | a list of the removed connected atom pair names
---|---

#### add_bonded_force

This **function** is used to add the bonded force to the residue type.


parameters | explanation
--- | ---
bonded_force_entity | the bonded force instance
typename | the bonded force type name. If None, get_class_name will be used to get the name


return | None
---|---

#### deepcopy

This **function** is used to deep copy the instance


parameters | explanation
--- | ---
name | the new ResidueType name
forcopy | the key to help you find who it is from


return | the new instance
---|---

### Entity

This **class** is the abstract class of the entities (atoms, bonded forces, residues and so on).


parameters | explanation
--- | ---
entity_type | the type of the entity
name | the name of the entity

#### get_class_name

This **function** gives the name of the class


return | the name of the class
---|---

#### update

This **function** is used to update the properties of the instance


parameters | explanation
--- | ---
kwargs | the properties to update


return | None
---|---

### Atom

This **class** is a subclass of Entity, for atoms


parameters | explanation
--- | ---
entity_type | a string or a AtomType instance, the type of the entity
name | the name of the entity

#### extra_excluded_atoms

the extra excluded atoms of this atom


return | a set of atoms
---|---

#### deepcopy

This **function** is used to deep copy the instance


parameters | explanation
--- | ---
forcopy | the key to help you find who it is from


return | the new instance
---|---

#### link_atom

This **function** is used to link atoms for building


parameters | explanation
--- | ---
link_type | the type to link
atom | the atom to link


return | None
---|---

#### extra_exclude_atom

This **function** is used to extra exclude one atom


parameters | explanation
--- | ---
atom | an Atom instance


return | None
---|---

#### extra_exclude_atoms

This **function** is used to extra exclude a list of atoms


parameters | explanation
--- | ---
atom_list | the atom list


return | None
---|---

### Residue

This **class** is a subclass of Entity, for residues


parameters | explanation
--- | ---
entity_type | a string or a ResidueType instance, the type of the entity
name | the name of the entity
directly_copy | if True, directly copy the Residue instance from the ResidueType instance

#### set_type

This **function** is used to change the type of the residue to a new type


parameters | explanation
--- | ---
new_type | the instance or the name of the new residue type
add_missing_atoms | whether to add missing atoms after deleting the terminal atoms


return | None
---|---

#### unterminal

This **function** is used to turn the terminal residue to be unterminal


parameters | explanation
--- | ---
add_missing_atoms | whether to add missing atoms after deleting the terminal atoms


return | 1 for success, 0 for failure
---|---

#### name2atom

This **function** convert an atom name to an Atom instance


parameters | explanation
--- | ---
name | the name


return | the Atom instance
---|---

#### atom2index

This **function** convert an Atom instance to its index


parameters | explanation
--- | ---
atom | the Atom instance


return | the index
---|---

#### name2index

This **function** convert an atom name to its index


parameters | explanation
--- | ---
name | the name


return | the index
---|---

#### add_atom

This **function** is used to add an atom to the residue


parameters | explanation
--- | ---
name | the name of the atom. If an Atom instance is given, only the name and type are used.
atom_type | the type of the atom. If None, it will copy from the ResidueType.
x | the coordinate x. If None, it will copy from the ResidueType.
y | the coordinate y. If None, it will copy from the ResidueType.
z | the coordinate z. If None, it will copy from the ResidueType.


return | None
---|---

#### add_connectivity

This **function** is used to add the connectivity between two atoms to the residue entity.


parameters | explanation
--- | ---
atom0 | the atom name or the Atom instance
atom1 | the atom name or the Atom instance


return | None
---|---

#### add_bonded_force

This **function** is used to add the bonded force to the residue entity.


parameters | explanation
--- | ---
bonded_force_entity | the bonded force instance


return | None
---|---

#### add_missing_atoms

This **function** is used to add the missing atoms from the ResidueType to the residue entity.


return | None
---|---

#### deepcopy

This **function** is used to deep copy the instance


parameters | explanation
--- | ---
forcopy | the key to help you find who it is from


return | the new instance
---|---

### ResidueLink

This **class** is a class for the link between residues


parameters | explanation
--- | ---
atom1 | the first atom to link
atom2 | the second atom to link

#### get_hash

This **function** is used to get the hash value of the ResidueLink


parameters | explanation
--- | ---
one | one Atom or Residue of the link
other | the other Atom or Residue of the link


return | the hash value
---|---

#### add_bonded_force

This **function** is used to add the bonded force to the residue link


parameters | explanation
--- | ---
bonded_force_entity | the bonded force instance


return | None
---|---

#### deepcopy

This **function** is used to deep copy the instance


parameters | explanation
--- | ---
forcopy | the key to help you find who it is from


return | the new instance
---|---

### Molecule

This **class** is a class for molecules


parameters | explanation
--- | ---
name | a string, the name of the Molecule or a ResidueType instance

#### cast

This **function** casts a Residue, a ResidueType or a Molecule to a Molecule


parameters | explanation
--- | ---
other | a Residue, a ResidueType or a Molecule instance
deepcopy | whether to deepcopy the other instance


return | a Molecule instance
---|---

#### set_save_sponge_input

This **function** is used to set the function when ``Save_SPONGE_Input``.


parameters | explanation
--- | ---
keyname | the file prefix to save


return | the decorator
---|---

#### del_save_sponge_input

This **function** is used to delete the function when ``Save_SPONGE_Input``.


parameters | explanation
--- | ---
keyname | the file prefix to save


return | None
---|---

#### set_mindsponge_todo

This **function** is used to set the function when ``Save_SPONGE_Input``.


parameters | explanation
--- | ---
keyname | the file prefix to save


return | the decorator
---|---

#### del_mindsponge_todo

This **function** is used to delete the function when ``Save_SPONGE_Input``.


parameters | explanation
--- | ---
keyname | the file prefix to save


return | None
---|---

#### find_spacious_direction

This **function** is used to find the most spacious (lowest atom density) direction of the givin point around the atom positions


parameters | explanation
--- | ---
point | a list of 3 numbers, the point to find the most spacious direction
atom_positions | the atom positions around the point. If None, this will use the positions of the atoms in this Molecule


return | a numpy array with the shape (3,), the most spacious direction
---|---

#### add_residue

This **function** is used to add a residue to the molecule


parameters | explanation
--- | ---
residue | the residue to add, either a Residue instance or a ResidueType instance


return | None
---|---

#### add_bonded_force

This **function** is used to add the bonded force to the residue type.


parameters | explanation
--- | ---
bonded_force_entity | the bonded force instance


return | None
---|---

#### add_residue_link

This **function** is used to add the connectivity between two atoms of two residues in the molecule.


parameters | explanation
--- | ---
atom1 | the first atom
atom2 | the second atom


return | None
---|---

#### get_residue_link

This **function** is used to get the ResidueLink between two residues


parameters | explanation
--- | ---
one | one Atom or Residue of the ResidueLink
other | the other Atom or Residue of the ResidueLink


return | the ResidueLink or None if not found
---|---

#### del_residue_link

This **function** is used to delete the ResidueLink between two residues


parameters | explanation
--- | ---
one | one Atom or Residue of the ResidueLink
other | the other Atom or Residue of the ResidueLink
key | "atom" or "residue", to specify the key


return | None
---|---

#### add_missing_atoms

This **function** is used to add the missing atoms from the ResidueType instances to the molecule.


return | None
---|---

#### set_missing_residues_info

This **function** is used to set the information about the missing residues


parameters | explanation
--- | ---
start | the residue or the residue index where the missing residues start. `None` for no starting residue.
end | the residue  or the residue index where the missing residues end. `None` for no ending residue.
missing_residues | the missing residues. The parameter can be a string separated by space, or a list of residue names, or a list of ResidueType or None. If None, the information will be deleted between start and end


return | True for success, False for failure to set
---|---

#### clear_missing_residues_info

This **function** is used to clear the information about the missing residues


return | None
---|---

#### add_missing_residues

This **function** is used to add the missing residues according to the information of the missing residues.


parameters | explanation
--- | ---
add_head | whether to add the missing residues at the head of the chain
add_tail | whether to add the missing residues at the tail of the chain


return | None
---|---

#### deepcopy

This **function** is used to deep copy the instance


return | the new instance
---|---

#### get_atoms

This **function** is used to get the atoms in the molecule.


return | a list of atoms
---|---

#### get_atom_coordinates

This **function** is used to get the atom coordinates


return | a numpy array, the coordinates of atoms
---|---

#### divide_into_two_parts

This **function** is used to divide the molecule into two parts


parameters | explanation
--- | ---
atom1 | the first atom
atom2 | the second atom


return | two numpy arrays, the index of the two parts
---|---

#### restrain_position

This **function** is used to generate the atoms for restraints


parameters | explanation
--- | ---
select | a string of MDAnalysis selection
filename | the name of the output atom index file


return | None
---|---

#### constrain_position

This **function** is used to generate the mass in file for constraints


parameters | explanation
--- | ---
select | a string of MDAnalysis selection
filename | the name of the output atom index file


return | None
---|---

### BondedForceEntity

This **class** is a subclass of Entity, for bonded forces

#### deepcopy

This **function** is used to deep copy the instance


return | None
---|---

### BondedForceType

This **class** is a subclass of Type, for bonded force types

#### same_force

This **function** receives a list of atoms and output all the same force permutations for the list


parameters | explanation
--- | ---
atom_list | 


return | None
---|---

### PairwiseForceType

This **class** is a subclass of Type, for pairwise force types

