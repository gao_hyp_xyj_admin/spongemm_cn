# Xponge.build

This **module** is used to build and save

## subpackages


## submodules


## functions

### build_bonded_force

This **function** build the bonded force for the input object


parameters | explanation
--- | ---
cls | the object to build


return | None
---|---

### get_mindsponge_system_energy

This **function** gets the system and energy for mindsponge


parameters | explanation
--- | ---
cls | the object to save, or a list of object to save
use_pbc | whether to use the periodic box conditions


return | a tuple, including the system and energy for mindsponge
---|---

### save_sponge_input

This **function** saves the iput object as SPONGE inputs


parameters | explanation
--- | ---
cls | the object to save
prefix | the prefix of the output files
dirname | the directory to save the output files


return | the molecule instance built
---|---

### save_pdb

This **function** saves the iput object as a pdb file


parameters | explanation
--- | ---
cls | the object to save
filename | the name of the output file


return | None
---|---

### save_mol2

This **function** saves the iput object as a mol2 file


parameters | explanation
--- | ---
cls | the object to save
filename | the name of the output file


return | None
---|---

### save_gro

This **function** saves the iput object as a gro file


parameters | explanation
--- | ---
cls | the object to save
filename | the name of the output file


return | None
---|---

