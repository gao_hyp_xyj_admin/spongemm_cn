# Xponge.process

This **module** is used to process topology and conformations

## subpackages


## submodules


## functions

### impose_bond

This **function** is used to impose the distance in ``molecule`` between ``atom1`` and ``atom2`` to ``length``


parameters | explanation
--- | ---
molecule | a ``Molecule`` instance
atom1 | the base atom, which will not change its coordinate
atom2 | the atom to change its coordinate to fit the length
length | distance in the unit of angstrom


return | None
---|---

### impose_angle

This **function** is used to impose the angle in ``molecule`` between ``atom1``, ``atom2`` and ``atom3`` to ``angle``.


parameters | explanation
--- | ---
molecule | a ``Molecule`` instance
atom1 | the base atom, which will not change its coordinate
atom2 | the base atom, which will not change its coordinate
atom3 | the atom to change its coordinate to fit the angle
angle | angle in the unit of rad


return | None
---|---

### impose_dihedral

This **function** is used to impose the dihedral in ``molecule`` between ``atom1``, ``atom2``, ``atom3`` and ``atom4`` to ``dihedral``.


parameters | explanation
--- | ---
molecule | a ``Molecule`` instance
atom1 | the base atom, which will not change its coordinate
atom2 | the base atom, which will not change its coordinate
atom3 | the atom to change its coordinate to fit the angle
atom4 | the atom to change its coordinate to fit the angle
dihedral | dihedral angle in the unit of rad
keep_atom3 | whether the other atoms linked to atom3 will be rotated


return | None
---|---

### add_solvent_box

This **function** adds a box full of solvents to a molecule.


parameters | explanation
--- | ---
molecule | the molecule to add the box, either a ``Molecule`` or a ``ResidueType``
solvent | the solvent molecule, either a ``Molecule`` or a ``ResidueType``
distance | the distance between the ``molecule`` and the box in the unit of Angstrom. This can be an ``int`` or a ``float``, and it can be also a list of them with the length 3 or 6, which represents the 3 or 6 directions respectively.
tolerance | the distance between two molecules. 2.5 for default.
n_solvent | the number of solvent molecules.


return | the Molecule instance
---|---

### h_mass_repartition

This **function** repartition the mass of light atoms to the connected heavy atoms. This can help the simulation run with a time step of 4 fs.


parameters | explanation
--- | ---
molecules | a ``Molecule``
repartition_mass | if the mass of the atom is not greater than this value, it will be seen as a light atom. 1.1 for default and in the unit of Dalton.
repartition_rate | The mass of the light atom will multiplied by this value.
exclude_residue_name | the residue name which will not do the repartition. "WAT" for default.


return | None
---|---

### solvent_replace

This **function** replaces the solvent to some other molecules randomly.


parameters | explanation
--- | ---
molecule | a ``Molecule`` instance
select | a **function** to decide which residues should be replaced, or a Residue, a ResidueType or a Molecule with only one Residue, which the residues to be replaced have the same name
toreplace | a dict, which stores the mapping of molecules to replace and the number of molecules. Every molecule should be a ``ResidueType``, a ``Residue`` or a ``Molecule`` with only one ``Residue``.
sort | whether to sort the residues after replacing


return | None
---|---

### main_axis_rotate

This **function** rotates the main axis of the molecule to the desired direction


parameters | explanation
--- | ---
molecule | a ``Molecule`` instance
direction_long | a list of three ``int`` or ``float`` to represent the direction vector. The long main axis will rotate to this direction.
direction_middle | a list of three ``int`` or ``float`` to represent the direction vector. The middle main axis will rotate to this direction.
direction_short | a list of three ``int`` or ``float`` to represent the direction vector. The short main axis will rotate to this direction.


return | None
---|---

### get_peptide_from_sequence

This **function** is used to get a peptide from the sequence


parameters | explanation
--- | ---
sequence | a string, the serial
charged_terminal | whether to change the terminal residues to the corresponding charged residue


return | a Molecule instance, the peptide
---|---

### optimize

This **function** is used to optimize the structure of the Molecule instance


parameters | explanation
--- | ---
mol | the molecule to optimize
step | the limited step for each epoch for minimization, 2000 for default
only_bad_coordinate | whether to optimize all the atoms or the atoms whose coordinates are bad
dt | the start dt for minimization
pbc | whether to use the periodic box condition
extra_commands | a dict, with the extra commands to pass to the MD engine


return | None
---|---

## classes

### Region

This **abstract class** is used to define a region

#### side

This **function** is used to set the side of the region"


parameters | explanation
--- | ---
side | either "in" or "out"


return | None
---|---

### IntersectRegion

This **class** is used to get the interset region of some regions


parameters | explanation
--- | ---
*regions | the regions

### UnionRegion

This **class** is used to get the union region of some regions


parameters | explanation
--- | ---
*regions | the regions

### BlockRegion

This **class** is used to define a block region


parameters | explanation
--- | ---
x_low | the lowest x coordinate of the block region
y_low | the lowest y coordinate of the block region
z_low | the lowest z coordinate of the block region
x_high | the highest x coordinate of the block region
y_high | the highest y coordinate of the block region
z_high | the highest z coordinate of the block region
side | either "in" or "out"
boundary | whether the boudary is seen as in the region

### SphereRegion

This **class** is used to define a sphere region


parameters | explanation
--- | ---
x | the x coordinate of the sphere origin
y | the y coordinate of the sphere origin
z | the z coordinate of the sphere origin
r | the radius of the sphere
side | either "in" or "out"
boundary | whether the boudary is seen as in the region

### FrustumRegion

This **class** is used to define a frustum region


parameters | explanation
--- | ---
x1 | the x coordinate of the first circle origin
y1 | the y coordinate of the first circle origin
z1 | the z coordinate of the first circle origin
r1 | the radius of the first circle origin
x2 | the x coordinate of the second circle origin
y2 | the y coordinate of the second circle origin
z2 | the z coordinate of the second circle origin
r2 | the radius of the second circle origin
side | either "in" or "out"
boundary | whether the boudary is seen as in the region

### PrismRegion

This **class** is used to define a prism (parallelepiped) region


parameters | explanation
--- | ---
x0 | the x coordinate of the origin
y0 | the y coordinate of the origin
z0 | the z coordinate of the origin
x1 | the x coordinate of the first basis vector
y1 | the y coordinate of the first basis vector
z1 | the z coordinate of the first basis vector
x2 | the x coordinate of the second basis vector
y2 | the y coordinate of the second basis vector
zz | the z coordinate of the second basis vector
x3 | the x coordinate of the third basis vector
y3 | the y coordinate of the third basis vector
z3 | the z coordinate of the third basis vector
side | either "in" or "out"
boundary | whether the boudary is seen as in the region

### Lattice

This **class** is used to help with the process of the lattice


parameters | explanation
--- | ---
style | the style of the lattice. "custom", "template:NAME" or the key values in Lattice.styles
basis_molecule | the unit to repeat, a Residue, a ResidueType or a Molecule
scale | the scale of the lattice
origin | a list with 3 numbers, the origin of the lattice. [0,0,0] for default.
cell_length | the length of the unit cell. [1,1,1] for default.
cell_angle | the angle of the unit cell. [90,90,90] for default.
basis_position | a list of lists, every sublist has 3 numbers for coordinates.
spacing | a list with 3 numbers, the spacing distance in three cell basis vectors.
periodic_bonds | a set of atom pair names. This can be from ResidueType.remove_periodic_connectivity
periodic_cutoff | a float. 3 for default. The cutoff of the bond to be recognized as periodic or not

#### create

This **function** is used to create basis molecules to the region in the box


parameters | explanation
--- | ---
box | the box of the system
region | the region to create the basis_molecule
mol | if ``mol`` the Molecule instance is provided, basis molecules will be added to ``mol``


return | a new Molecule instance, or the Molecule instance ``mol``
---|---

