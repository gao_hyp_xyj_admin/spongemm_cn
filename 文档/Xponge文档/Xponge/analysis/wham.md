# Xponge.analysis.wham

This **module** implements the WHAM method

## subpackages


## submodules


## functions

## classes

### WHAM

do the WHAM analysis


parameters | explanation
--- | ---
window_edges | the edge of the windows
temperature | the temperature
weight | the weight of the bias
references | the references of the bias
period | the period of the CV
step_limit | the maximum step to calculate the free energy
diff_limit | stop iterations when the difference reaches the diff_limit

#### get_data_from_mdout

get the CV information from the mdout files


parameters | explanation
--- | ---
mdouts | the file name of mdouts
cv_name | the name of the CV

#### bias

the function to get the bias

