# Xponge.analysis.sasa

This **module** implements the SASA calculation

## subpackages


## submodules


## functions

## classes

### SASA

This **class** implements the SASA calculation


parameters | explanation
--- | ---
u | the instance of the MDAnalysis universe
r_probe | radius of the probe
n_points | resolution of the surface of each atom
radii_dict | dict of atomic radii to update the default RADII dict
r_atoms | if specify, the atomic radii will directly use the value here instead of looking up in radii_dict.

#### main

This **function** does the real calculation.


parameters | explanation
--- | ---
need_area | whether keep the area. If True, the surface will stored in self.surface_area
need_surface | whether need the surface. If True, the surface will stored in self.surface

#### write_surface_xyz

This **function** writes the surface information to a file


parameters | explanation
--- | ---
filename | the name of the output file
headlines | whether the headlines and fake elements are written to the file

#### get_sasa_result

This **function** gets the sasa result


parameters | explanation
--- | ---
select | the sasa  sum of the selected atoms

