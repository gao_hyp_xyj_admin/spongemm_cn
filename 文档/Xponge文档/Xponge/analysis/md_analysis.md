# Xponge.analysis.md_analysis

This **module** gives functions and classes to use MDAnalysis to analyze the trajectories

## subpackages


## submodules


## functions

### open_trajectory

Open the trajectory file


return | trajectory file and box file
---|---

## classes

### SpongeNoneReader

This **class** is used to give a universe with no coordinate

#### close

fake close function for api

### SpongeInputReader

This **class** is used to read the SPONGE input to mdanalysis

#### parse

This **function** reads the file and returns the structure


parameters | explanation
--- | ---
kwargs | keyword arguments


return | MDAnalysis Topology object
---|---

### XpongeResidueReader

This **class** is used to read the Xponge Residue or ResidueType to mdanalysis

#### parse

This **function** reads the file and returns the structure


parameters | explanation
--- | ---
kwargs | keyword arguments


return | MDAnalysis Topology object
---|---

### XpongeMoleculeReader

This **class** is used to read the Xponge Molecule to mdanalysis

#### parse

This **function** reads the file and returns the structure


parameters | explanation
--- | ---
kwargs | keyword arguments


return | MDAnalysis Topology object
---|---

### SpongeTrajectoryReader

This **class** is the interface to MDAnalysis.


parameters | explanation
--- | ---
dat_file_name | the name of the SPONGE dat trajectory file
box | the name of the box file or a list of 6 ``int`` or ``float`` representing the 3 box lengths and 3 box angles.
n_atoms | the number of atoms

#### n_frames

The total number of frames in the trajectory file

#### n_atoms

The total number of atoms in the trajectory file

#### with_arguments

This **function** binds the arguments to the reader to initialize


parameters | explanation
--- | ---
kwargs | the arguments


return | a subclass of SpongeTrajectoryReader
---|---

### SpongeTrajectoryReaderWithArguments



### SpongeTrajectoryWriter

This **class** is used to write the SPONGE trajectory (xxx.dat and xxx.box)


parameters | explanation
--- | ---
filename | the filename of the output files
write_box | whether to write the box file **New From 1.2.7.0**

#### open

This **function** opens the trajectory files


return | None
---|---

#### close

This **function** closes the trajectory files


return | None
---|---

#### write

This **function** writes the coordinates of the Universe to the output files


parameters | explanation
--- | ---
u | an MDAnalysis.Universe instance


return | None
---|---

### SpongeCoordinateReader

This **class** is the interface to MDAnalysis.


parameters | explanation
--- | ---
file_name | the name of the SPONGE coordinate trajectory file

#### n_frames

The total number of frames in the trajectory file

#### n_atoms

The total number of atoms in the trajectory file

#### close

Close all the opened file


return | None
---|---

#### open_file

Open the coordinate file


return | trajectory file and box file
---|---

### SpongeCoordinateWriter

This **class** is used to write the SPONGE coordinate file


parameters | explanation
--- | ---
file_name | the name of the output file
n_atoms | the total number of atoms this Timestep describes

#### open

This **function** opens the coordinate file


return | None
---|---

#### close

This **function** closes the coordinate file


return | None
---|---

#### write

This **function** writes the coordinates of the Universe to the output files


parameters | explanation
--- | ---
u | an MDAnalysis.Universe instance


return | None
---|---

### SPONGEH5MDReader

This **class** is the interface to MDAnalysis.


parameters | explanation
--- | ---
dat_file_name | the name of the SPONGE h5md trajectory file
n_atoms | the number of atoms

#### n_frames

The total number of frames in the trajectory file

#### n_atoms

The total number of atoms in the trajectory file

#### close

Close all the opened file


return | None
---|---

