# Xponge.tools

This **module** implements the terminal commands

## subpackages


### [Xponge.tools.unittests](/文档/Xponge文档/Xponge/tools/unittests)


## submodules


### [Xponge.tools.mbar](/文档/Xponge文档/Xponge/tools/mbar)


### [Xponge.tools.mmgbsa](/文档/Xponge文档/Xponge/tools/mmgbsa)


### [Xponge.tools.mol2rfe](/文档/Xponge文档/Xponge/tools/mol2rfe)


### [Xponge.tools.ti](/文档/Xponge文档/Xponge/tools/ti)


## functions

### converter

This **function** converts the format of coordinate file


parameters | explanation
--- | ---
args | arguments from argparse


return | None
---|---

### maskgen

This **function** uses VMD to generate mask


parameters | explanation
--- | ---
args | arguments from argparse


return | None
---|---

### exgen

This **function** reads the SPONGE input files for bonded interactions and generate a exclude file


parameters | explanation
--- | ---
args | arguments from argparse


return | None
---|---

### name2name

This **function** change the atom names from one file to another file


parameters | explanation
--- | ---
args | arguments from argparse


return | None
---|---

### mol2rfe

This **function** helps with the relative free energy calculation


parameters | explanation
--- | ---
args | arguments from argparse


return | None
---|---

### mm_gbsa

This **function** helps with the MM/GBSA calculation


parameters | explanation
--- | ---
args | arguments from argparse


return | None
---|---

