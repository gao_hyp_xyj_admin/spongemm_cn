---
title: Windows下的安装
description: 
published: true
date: 2024-01-01T15:12:35.700Z
tags: 
editor: markdown
dateCreated: 2023-12-29T01:05:21.155Z
---

# Windows下SPONGE及配套软件的安装

> 更新时间
> 2024/01/01
{.is-info}

## 引入

本教程包含在Windows操作系统下的SPONGE及其配套软件的安装。涉及的软件有：

- SPONGE：分子动力学模拟主程序
- Xponge：分子动力学前后处理软件
- VMD：分子动力学可视化软件

## 从源码安装SPONGE

### 确认所需软件的版本

先根据自己的显卡和驱动查看合适的cuda版本，此处打开NVIDIA控制面板

![19.png](/教程/安装/19.png)

点击其中的帮助-系统信息

![20.png](/教程/安装/20.png)

在其组件部分可以看到对应的cuda支持版本

![18.png](/教程/安装/18.png)

随后确定自己所需的编译器visual studio版本，目前无简单判断方式，可通过搜索相应关键词，如我的11.2可直接搜索

![33.png](/教程/安装/33.png)

查看到有其他人使用的例子为visual studio 2019

![34.png](/教程/安装/34.png)

### 编译器的安装

在Windows下，推荐使用Visual Studio作为编译器，从官方网址[Visual Studio 2019 版本 16.11 发行说明 | Microsoft Docs](https://docs.microsoft.com/zh-cn/visualstudio/releases/2019/release-notes)下载即可，其中社区（community）版Visual Studio对于个人用户是免费的。此处的版本可以在该页面旁边进行更改。

![1.png](/教程/安装/1.png)

接下来以Visual Studio 2022版本为例。当下载完成后打开，点击继续

![](/教程/安装/2.png)

然后等待一阵下载和安装设置后，来到工作负荷界面，选择C++桌面开发工具

![](/教程/安装/3.png)

随后即进入下载安装界面，等待下载安装完成

![](/教程/安装/4.png)

![](/教程/安装/5.png)

可不先重启，直接打开即可，并暂时跳过登录

![](/教程/安装/6.png)

配置一些外观设置

![](/教程/安装/7.png)

当能够看到下面界面的时候Visual Studio即安装成功

![](/教程/安装/8.png)

### CUDA的安装

前往cuda官网[CUDA Toolkit Archive | NVIDIA Developer](https://developer.nvidia.com/cuda-toolkit-archive)下载，按照提示根据你的情况选择正确的版本，以及平台信息，选择local安装器，点击Download即可

![](/教程/安装/9.png)

下载完成后启动安装器，选择合适的临时文件存储文件夹

![](/教程/安装/10.png)

![](/教程/安装/11.png)

同意使用协议

![](/教程/安装/12.png)

在安装选项界面，选择自定义安装

![](/教程/安装/13.png)

只需要勾选CUDA下的Development、Runtime和Visual Studio Integration。其他部分按照个人需求勾选。

![](/教程/安装/14.png)

选择合适的安装位置

![](/教程/安装/15.png)

开始安装

![](/教程/安装/16.png)

安装结束

![](/教程/安装/17.png)

### SPONGE程序包的安装

前往SPONGE官网[SPONGE (spongemm.cn)](/下载/CudaSPONGE程序)下载

![](/教程/安装/23.png)

下载完成后，是一个zip文件包，将其解压以后，打开其中的SPONGE文件夹，其中包含一个bat文件`vs_project_generator.bat`，双击运行该文件。中途可能会有防火墙提示，点击仍要运行

![](/教程/安装/21.png)

该脚本会自动生成工程文件，弹出下面的提示，按任意键退出

![](/教程/安装/22.png)

回到SPONGE文件夹中，可以看到此时出现了下面的文件

![](/教程/安装/24.png)

相应的工程文件对应了SPONGE的主程序。因为兼容性的考虑，生成的工程文件是visual studio 2013，因此使用较新的visual studio打开后会提示升级，点击确定即可。

![](/教程/安装/26.png)

点击项目-重定目标解决方案，然后再次点击确定，即可升级到你版本的visual studio

![](/教程/安装/27.png)

![](/教程/安装/28.png)

随后点击上方的生成Windows调试器即可

![](/教程/安装/32.png)

最后会运行一个程序的黑窗口，出现以下界面即为编译成功

![](/教程/安装/35.png)

最后生成的可执行文件存在SPONGE/x64/Release中，将该文件夹设置到环境变量中即可

![](/教程/安装/36.png)

## Xponge的安装

### miniconda的安装

前往官网[Miniconda — conda documentation](https://docs.conda.io/en/latest/miniconda.html)下载最新版miniconda

![](/教程/安装/37.png)

随后一直默认选项安装即可

![](/教程/安装/38.png)

### Xponge的安装

打开miniconda 的终端，因为中途可能有测试文件的产生，因此请先将目录换到一个空的临时文件夹中

![](/教程/安装/39.png)

逐行运行以下命令安装Xponge并测试是否成功

```
conda create -n Xponge python==3.7.5 -y
conda activate Xponge
pip install Xponge
Xponge test
```

如果最终出现

```bash
3 test case(s) for base - base
...
```

那么即安装成功

### 将SPONGE与Xponge关联

逐行运行以下命令（其中`D:\SPONGE\SPONGE\x64\Release`请修改为你自己的SPONGE文件夹）

```
Xponge.mdrun -set D:\SPONGE\SPONGE\x64\Release
Xponge.mdrun SPONGE
del mdinfo.txt mdout.txt
```

得到下列结果即绑定成功

![](/教程/安装/41.png)

## VMD的安装

### VMD主程序的安装

前往vmd官网[VMD - Visual Molecular Dynamics (uiuc.edu)](https://www.ks.uiuc.edu/Research/vmd/)，此处推荐下载最新的1.9.4版本

![](/教程/安装/42.png)

![](/教程/安装/43.png)

![](/教程/安装/44.png)

![](/教程/安装/45.png)

会需要你注册，此时你随意注册即可

![](/教程/安装/46.png)

![](/教程/安装/47.png)

![](/教程/安装/48.png)

### 安装SPONGE的VMD插件

在SPONGE官网[SPONGE (spongemm.cn)](/下载/SPONGE工具)下载vmd插件

下载后是一个zip文件，解压后可自行阅读其中的`README`文件进行配置，最终打开VMD，并在新文件格式中找到SPONGE相关格式即为安装成功

![](/教程/安装/50.png)