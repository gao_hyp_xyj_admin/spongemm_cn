---
title: Linux下的安装
description: 
published: true
date: 2024-01-01T15:12:08.981Z
tags: 
editor: markdown
dateCreated: 2023-12-29T00:59:05.852Z
---

# Linux下SPONGE及配套软件的安装

> 更新时间
> 2024/01/01
{.is-info}

## 引入

本教程包含在Ubuntu 18.04（包含可视化桌面）操作系统下的SPONGE及其配套软件的安装。涉及的软件有：

- CudaSPONGE：分子动力学模拟主程序
- Xponge：分子动力学前后处理软件
- VMD：分子动力学可视化软件

## 从源码安装CudaSPONGE

### 确认CUDA版本

正常情况下，拥有可视化桌面的系统是已经安装有显卡驱动的，而卸载显卡驱动可能导致很多复杂问题，一般根据你的显卡驱动版本去寻找可以使用的CUDA版本。

可以使用命令`nvidia-smi`获取驱动版本和支持的最高CUDA版本。

```bash
nvidia-smi
```



```bash
Sat Sep 24 12:27:21 2022       
+-----------------------------------------------------------------------------+
| NVIDIA-SMI 460.141.03  Driver Version: 460.141.03    CUDA version: 11.2     |
|-------------------------------+----------------------+----------------------+
| GPU  Name        Persistence-M| Bus-Id        Disp.A | Volatile Uncorr. ECC |
| Fan  Temp  Perf  Pwr:Usage/Cap|         Memory-Usage | GPU-Util  Compute M. |
|===============================+======================+======================|
|   0  Quadro RTX 4000     On   | 00000000:17:00.0 Off |                  N/A |
| 30%   38C    P8     8W / 125W |    173MiB /  8192MiB |      0%      Default |
+-------------------------------+----------------------+----------------------+
```

可见我现在操作的电脑最高支持的版本是11.2。

另外部分低版本驱动下的nvidia-smi不显示可以使用的CUDA版本，可以前往[Release Notes :: CUDA Toolkit Documentation (nvidia.com)](https://docs.nvidia.com/cuda/cuda-toolkit-release-notes/index.html#cuda-major-component-versions__table-cuda-toolkit-driver-versions)网站查询支持的显卡驱动版本和CUDA版本。

### 编译器的安装

正确情况下，Linux系统下自带gcc编译器，不需要额外设置，除非你的操作系统过老，以致于gcc版本也太低，此时需升级gcc编译器。

### 下载CUDA

前往cuda官网[CUDA Toolkit Archive | NVIDIA Developer](https://developer.nvidia.com/cuda-toolkit-archive)下载，按照提示根据你的情况选择正确的版本，以及平台信息，选择local安装器，按照下方的提示在命令行中输入命令即可

![](/教程/安装/l1.png)

![](/教程/安装/l2.png)

正常情况下，安装时只勾选toolkit即可。

![](/教程/安装/l3.png)

随后需要设置环境变量。Ubuntu默认的环境变量在`~/.bashrc`中设置，可使用下列代码设置

```bash
echo 'export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/cuda-11.2/lib64:/usr/local/cuda-11.2/lib' >> ~/.bashrc
echo 'export PATH=$PATH:/usr/local/cuda-11.2/bin' >> ~/.bashrc
```

其中的`cuda-11.2`更改为你安装的对应的cuda版本

安装完毕后输入`nvcc -V`，不报错即为安装成功

```bash
nvcc -V
```

```bash
nvcc: NVIDIA (R) Cuda compiler driver
Copyright (c) 2005-2020 NVIDIA Corporation
Built on Mon_Nov_30_19:08:53_PST_2020
Cuda compilation tools, release 11.2, V11.2.67
Build cuda_11.2.r11.2/compiler.29373293_0
```

### SPONGE程序包的安装

前往SPONGE官网[SPONGE (spongemm.cn)](/下载/CudaSPONGE程序)下载

![](/教程/安装/23.png)

下载完成后，是一个zip文件包，使用`unzip`命令将其解压以后，打开其中的SPONGE文件夹，使用以下命令安装，其中12是指用12个线程进行编译

```bash
make install -j 12
```

安装好以后，在该文件夹下直接运行`./SPONGE`命令，能够正常运行即为安装成功

## Xponge的安装

### Xponge的安装

前往官网[Miniconda — conda documentation](https://docs.conda.io/en/latest/miniconda.html)下载安装最新版miniconda，随后使用以下命令

注意，miniconda的安装不是必须的，你可以直接从pip install Xponge一步开始进行。

```bash
conda create -n Xponge python -y
conda activate Xponge
pip install Xponge
Xponge test
```

如果最终出现

```bash
3 test case(s) for base - base
...
```

那么即安装成功。

### 将SPONGE与Xponge关联

逐行运行以下命令（其中`~/Desktop/SPONGE`请修改为你自己的SPONGE文件夹）

```bash
Xponge.mdrun -set ~/Desktop/SPONGE
Xponge.mdrun SPONGE
rm mdinfo.txt mdout.txt
```

得到下列结果即绑定成功

![41.png](/教程/安装/41.png)

## VMD的安装

### VMD主程序的安装

前往vmd官网[VMD - Visual Molecular Dynamics (uiuc.edu)](https://www.ks.uiuc.edu/Research/vmd/)，此处推荐下载最新的1.9.4版本

![](/教程/安装/42.png)

![](/教程/安装/43.png)

一直点击，下载即可，下载后会得到一个压缩包，解压以后打开进入解压的文件夹中，然后依次运行下列命令

```bash
sudo ./configure LINUXAMD64 #LINUXAMD64 can be replaced according to your OS
./configure
cd src
sudo make install
```

### 安装SPONGE的VMD插件

在SPONGE官网[SPONGE (spongemm.cn)](/下载/SPONGE工具)下载vmd插件

下载后是一个zip文件，解压后可自行阅读其中的`README`文件进行配置，最终打开VMD，并在新文件格式中找到SPONGE相关格式即为安装成功

![](/教程/安装/50.png)

 